("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {
      var isMobile = window.innerWidth < 768 ? true : false;
      let base_url = window.location.origin;

      if (window.innerWidth > 768) {
        const mainMenu = document.querySelector(".main-menu");
        const tracker = document.createElement("span");
        tracker.classList.add("selected");
        mainMenu.append(tracker);
        const anchors = Array.from(
          mainMenu.querySelectorAll(
            `.menu-item a[href^="#"], .menu-item a[href^="${base_url}/#"]`
          )
        );

        if (anchors.length !== 0) {
          const target = anchors
            .map((a) => {
              return document.querySelector("#" + a.href.split("#")[1]);
            })
            .filter((e) => e !== null && e !== undefined);
          let lastHover = mainMenu.querySelector(".menu-item");

          function setPos(el) {
            const cursor = mainMenu.querySelector(".selected");
            if (window.innerWidth > 768) {
              if (
                document
                  .querySelector("header")
                  .classList.contains("header-light") &&
                el === mainMenu.querySelector(".menu-item:last-of-type")
              ) {
                cursor.setAttribute(
                  "style",
                  `width: ${el.clientWidth}px; left: ${el.offsetLeft}px; opacity: 0;`
                );
              } else {
                cursor.setAttribute(
                  "style",
                  `width: ${el.clientWidth}px; left: ${el.offsetLeft}px; opacity: 1`
                );
              }
            }
          }

          setPos(lastHover);
          const vh = window.innerHeight;
          let lastId = "home";
          window.addEventListener("scroll", () => {
            let fromTop = window.scrollY;
            let cur = target.filter(
              (t) => t.getBoundingClientRect().y + fromTop < fromTop + vh / 2
            );
            cur = cur[cur.length - 1];
            let id = cur !== undefined ? cur.id : "";
            if (id !== lastId) {
              lastId = id;
              if (lastId)
                lastHover = mainMenu.querySelector(
                  `.menu-item a[href="${base_url}/#${lastId}"], .menu-item a[href="#${lastId}"]`
                ).parentElement;
              setPos(lastHover);
            }
          });
          mainMenu
            .querySelectorAll(".menu-item")
            .forEach((m) => m.addEventListener("mouseover", () => setPos(m)));
          document
            .querySelector(".main-menu-container")
            .addEventListener("mouseleave", () => setPos(lastHover));
        }
      }

      var lastScrollTop = 0;
      $(window).scroll(function () {
        var st = $(this).scrollTop();
        if (st > 100) {
          if (st > lastScrollTop) {
            $("body").addClass("down-scroll");
            $("body").removeClass("up-scroll");
            if (st > 100 && !$("body").hasClass("sticky")) {
              $("body").addClass("sticky");
            }
          } else {
            $("body").addClass("up-scroll");
            $("body").removeClass("down-scroll");
            if (
              st < $(window).height() - 50 &&
              $("body").hasClass("sticky") &&
              !$("body").hasClass("f-sticky")
            ) {
              $("body").removeClass("sticky");
            }
          }
          lastScrollTop = st;
        }
      });

      /**
       *   Blog
       */

      $(".single-link-container").each(function () {
        let bkg = $(this).find(".archive-img-cover").attr("src");
        let target = $(".attachment-post-thumbnail");

        $(this).on("mouseover", function () {
          if (target.attr("src") != bkg) {
            TweenMax.to(".attachment-post-thumbnail", 0.3, {
              autoAlpha: 0,
              scale: 1.05,
            });

            target.attr("src", bkg);

            setTimeout(() => {
              TweenMax.to(".attachment-post-thumbnail", 0.3, {
                autoAlpha: 1,
                scale: 1,
              });
            }, 300);
          }
        });
      });

      if (!isMobile) {
        $('.single-link-container[parallax="1"]').each(function () {
          var parallaxController = new ScrollMagic.Controller({});
          new ScrollMagic.Scene({
            triggerElement: ".container-index",
            triggerHook: 0.65,
            duration: "100%",
          })
            .setTween($(this), { y: -150, ease: Linear.easeNone })
            .addTo(parallaxController);
        });

        $(".single-parallax").each(function () {
          var parallaxController = new ScrollMagic.Controller({});
          new ScrollMagic.Scene({
            triggerElement: ".single-container",
            triggerHook: 0.65,
            duration: "100%",
          })
            .setTween($(this), { y: -150, ease: Linear.easeNone })
            .addTo(parallaxController);
        });
      }

      /**
       *  CPT Archive & Single
       */

      $(".filtre-toggle").each(function () {
        $(this).click(function () {
          $(".filtre").toggleClass("open");
        });
      });

      $(".archive-single-product").each(function () {
        let bkg = $(this).find(".wp-post-image").attr("src");
        let target = $(".attachment-post-thumbnail.archive-bkg");

        $(this).on("mouseover", function () {
          if (target.attr("src") != bkg) {
            TweenMax.to(".attachment-post-thumbnail.archive-bkg", 0.3, {
              autoAlpha: 0,
              scale: 1.05,
            });

            target.attr("src", bkg);

            setTimeout(() => {
              TweenMax.to(".attachment-post-thumbnail.archive-bkg", 0.3, {
                autoAlpha: 1,
                scale: 1,
              });
            }, 300);
          }
        });
      });

      $(".desc-tab-tgl").click(function () {
        $(".desc-tab").removeClass("d-none").addClass("d-block");
        $(".additional-tab").removeClass("d-block").addClass("d-none");
        $(this).addClass("active");
        $(".additional-tab-tgl").removeClass("active");
      });
      $(".additional-tab-tgl").click(function () {
        $(".additional-tab").removeClass("d-none").addClass("d-block");
        $(".desc-tab").removeClass("d-block").addClass("d-none");
        $(this).addClass("active");
        $(".desc-tab-tgl").removeClass("active");
      });

      $(".product-gallery-sticky").each(function () {
        $(this).slick({
          autoplay: false,
          dots: true,
          fade: false,
          infinite: false,
        });
      });

      /**
       *   FAQ GUTENBERG
       */
      $(".schema-faq-question").click(function () {
        $(this).parent().find(".schema-faq-answer").slideToggle(300);
        $(this).toggleClass("question-open");
      });

      /**
       *  notification / modal
       */
      const modals = document.querySelectorAll(".notification, .custom-modal");
      modals.forEach((modal) => {
        const delay = modal.dataset.delay || 0;
        console.log(parseInt(sessionStorage.getItem('modal')), Date.now(), Date.now() > parseInt(sessionStorage.getItem('modal')));
        if (
          sessionStorage.getItem("modal") === null ||
          Date.now() > new Date(parseInt(sessionStorage.getItem("modal")))
        ) {
          setTimeout(() => {
            modal.classList.remove('closed');
            if(modal.classList.contains('custom-modal')) document.body.classList.add('modalOpen')
          }, delay);

          const closeBtns = modal.querySelectorAll(".close-modal");
          closeBtns.forEach((btn) => {
            btn.addEventListener('click', () => {
              modal.classList.add('closed');
              if(modal.classList.contains('custom-modal')) document.body.classList.remove('modalOpen');
              const expiration_date = new Date(Date.now() + 24 * 60 * 60 * 1000);
              sessionStorage.setItem("modal", expiration_date.getTime());
            });
          });

        }
      });

      /**
       * Burger menu toggle
       */
      $(".menu-toggle").each(function () {
        $(this).click(function () {
          $("nav.mobile-main-menu").toggleClass("nav-open");
        });
      });
      if (isMobile) {
        $(".menu-item a").click(function () {
          $("nav.mobile-main-menu").removeClass("nav-open");
        });
      }

      const filter = document.querySelector(".filtre-toggle");
      if (filter) {
        setTimeout(() => filter.classList.add("obvious"), 2000);

        filter.addEventListener("mouseover", () =>
          filter.classList.remove("obvious", "obvious2")
        );
        filter.addEventListener("mouseleave", () =>
          setTimeout(() => filter.classList.add("obvious2"), 2000)
        );
      }

      const zIndexElement = document.querySelectorAll("[class*=z-index-]");
      zIndexElement.forEach((e) => {
        const value = e.classList.value.split("z-index-", 2)[1].split(" ", 1);
        e.style.setProperty("--index", value);
        e.classList.add("z-index-x");
      });

      
    });
  });
})(jQuery, this);
