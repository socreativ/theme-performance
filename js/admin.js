"use strict";

docReady(() => {
    if(!admin_data.isAdmin) disabledShopActivation();
});

function disabledShopActivation() {
  const shopActivation = document.getElementById('shop_activation');
  shopActivation.classList.add('disabled');
  shopActivation.append(notifyRightAccess());
}

function notifyRightAccess(){
    const p = document.createElement('p');
    p.classList.add('notify-right-access');
    p.innerHTML = 'Vous ne pouvez pas activer/désactiver cette option vous même. Contactez un administrateur.';
    return p;
}

function docReady(fn) {
  // see if DOM is already available
  if (document.readyState === "complete" || document.readyState === "interactive") {
    // call on next available tick
    setTimeout(fn, 1);
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}