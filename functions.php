<?php
/**
 * theme-by-socreativ functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package theme-by-socreativ
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}


if ( ! function_exists( 'theme_by_socreativ_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function theme_by_socreativ_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on theme-by-socreativ, use a find and replace
		 * to change 'theme-by-socreativ' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'theme-by-socreativ', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'theme_by_socreativ_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'theme_by_socreativ_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function theme_by_socreativ_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'theme_by_socreativ_content_width', 640 );
}
add_action( 'after_setup_theme', 'theme_by_socreativ_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_by_socreativ_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'theme-by-socreativ' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'theme-by-socreativ' ),
			'before_widget' => '<section id="%1$s" class="%2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar( array(
        'name'          => __( 'Sub Footer', 'theme-by-socreativ' ),
        'id'            => 'sub-footer-widget',
        'description'   => __( 'Add widgets for your blog here.', 'theme-by-socreativ' ),
        'before_widget' => '<section id="%1$s" class="%2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title h5">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'theme_by_socreativ_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function theme_by_socreativ_scripts() {
	wp_enqueue_style( 'theme-by-socreativ-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'theme-by-socreativ-style', 'rtl', 'replace' );

	/** SoCreativ styles + bootstrap  **/
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.custom.min.css', true, '1.0', 'all');
	wp_enqueue_style( 'helpers', get_stylesheet_directory_uri() . '/assets/css/helpers.css', true, '1.0', 'all');
	wp_enqueue_style( 'header', get_stylesheet_directory_uri() . '/assets/css/header.css', true, '1.0', 'all');
	wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/assets/css/main.css', true, '1.0', 'all');
	wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/assets/css/footer.css', true, '1.0', 'all');
	wp_enqueue_style( 'blog', get_stylesheet_directory_uri() . '/assets/css/blog.css', true, '1.0', 'all', false);
	wp_enqueue_style( 'CustomPostType', get_stylesheet_directory_uri() . '/assets/css/cpt.css', true, '1.0', 'all', false);
	wp_enqueue_style( 'Gutemberg', get_stylesheet_directory_uri() . '/assets/css/gutemberg.css', true, '1.0', 'all', false);
	wp_enqueue_style( 'responsive', get_stylesheet_directory_uri() . '/assets/css/responsive.css', true, '1.0', 'all', false);

	/* Fancy box */
	wp_enqueue_style( 'fancyBoxStyle', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', true, '1.0', 'all');
	wp_enqueue_script( 'fancyBoxScript', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', ['jquery'], '1.0.0', false);

	/** Slick Slider   **/
	wp_enqueue_style( 'slickSliderStyle', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css', true, '1.0', 'all');
	wp_enqueue_style( 'slickSliderTheme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css', true, '1.0', 'all');
	wp_enqueue_script( 'slickSlider', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', ['jquery'], '1.0.0', false);


	/** Tweenmax + ScrollMagic   **/
	wp_enqueue_script( 'tweenmax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', ['jquery'], '1.0.0', true);
	//wp_enqueue_script( 'attrplugin.gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/AttrPlugin.min.js', ['jquery'], '1.0.0', false);
	wp_enqueue_script( 'ScrollToPlugin', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js', ['jquery'], '1.0.0', true);
	wp_enqueue_script( 'scrollmagic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/ScrollMagic.min.js', ['jquery'], '1.0.0', true, false);
	//wp_enqueue_script( 'scrollmagicaddIndicators', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/debug.addIndicators.min.js', ['jquery'], '1.0.0', false);
	wp_enqueue_script( 'animation.gsap', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/animation.gsap.min.js', ['jquery'], '1.0.0', true);
	//wp_enqueue_script( 'mousewheel', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js', array(), _S_VERSION, true );

	/** SoCreativ scripts  **/
	wp_enqueue_script( 'CheckBrowser', get_template_directory_uri() . '/assets/js/browser.js', array(), '1.0.0', true);
	wp_enqueue_script( 'theme-by-socreativ-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'globalScript', get_template_directory_uri() . '/assets/js/script.js', ['jquery'], '1.0.0', false);


	if(is_plugin_active('woocommerce/woocommerce.php')){
		wp_enqueue_style( 'woocommerce-support', get_stylesheet_directory_uri() . '/assets/css/woocommerce.css', true, '1.0', 'all', false);
	}

	if (have_rows('premier_niveau')) {
		wp_enqueue_script( 'bootstrapjs', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js', array(), '1.0.0', true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'theme_by_socreativ_scripts' );


// ALLOW SVG uploads
/**
 * This method allows you to add custom mime types.
 * For example, you can add svg mime types to load svg image
 *
 * @param $mimes
 * @return mixed
 */
function upload_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'upload_mime_types');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * 	Create custom thumbnail size
 */

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'square-thumb', 400, 400, true ); // 300 pixels wide (and unlimited height)
}

/* -------------------------------------------
				GUTENBERG STYLE
--------------------------------------------- */

// Gutenberg Editor width
function gb_gutenberg_admin_styles() {
    echo '
        <style>
            /* Main column width */
            .wp-block {
                max-width: 1220px;
            }

            /* Width of "wide" blocks */
            .wp-block[data-align="wide"] {
                max-width: 1980px;
            }

            /* Width of "full-wide" blocks */
            .wp-block[data-align="full"] {
                max-width: none;
            }
        </style>
    ';
}
add_action('admin_head', 'gb_gutenberg_admin_styles');



/*********************************************************************
                            Custom Menu
 ********************************************************************/

// Create custom main menu
function wpb_custom_new_menu() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main menu' ),
			'footer' => __( 'Footer '),
			'RS' => __('Réseau Sociaux')
		)
	);
}
add_action( 'init', 'wpb_custom_new_menu' );

// Get ACF image field and create new attribute to object
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
function my_wp_nav_menu_objects( $items, $args ) {
	foreach( $items as &$item ) {
		$image = get_field('menu_image', $item);
		$icon = get_field('menu_icon', $item);
		if( $image ) {
			$item->attr_img = $image['url'];
			$item->attr_img_item = 'image-item';
		}
		if( $icon ) {
			$item->icon = '<img class="" src="'. $icon['url'] . '" alt="' . $icon['alt'] . '"/>';
		}
		$item->title = $item->icon . '<span class="anim-300">' . $item->title . '</span>';
	}
	return $items;
}



// If object had new 'img' attribute, insert it into anchor element
add_filter( 'nav_menu_link_attributes', 'wpse_100726_extra_atts', 10, 3 );
function wpse_100726_extra_atts( $atts, $item, $args ){
		$atts['data-img'] = $item->attr_img;
		$atts['data-item'] = $item->attr_img_item;
		return $atts;
}

// Add link to admin backoffice in footer
add_filter('wp_nav_menu_items', 'add_admin_link', 10, 2);
function add_admin_link($items, $args){
    if( $args->theme_location == 'footer' ){
        $items .= '<li><a title="Admin" href="'. esc_url( admin_url() ) .'">' . __( 'Admin' ) . '</a></li>';
    }
    return $items;
}


/*********************************************************************
                            ACF Params
 ********************************************************************/


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' => __('Options'),
		'menu_title' => __('Options'),
		'menu_slug' => 'options',
		'position' => '31'
	));
}

add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

	/* Add gategorie for blocks */
	function my_block_category($categories, $post){
		return array_merge(
			$categories,
			array(
				array(
					'slug' => 'custom-blocks',
					'title' => __('Custom Block', 'custom-blocks'),
				),
			)
		);
	}
	add_filter('block_categories', 'my_block_category', 10, 2);

	//custom block ACF
	require 'inc/customBlocks.php';

	//custom post Type ACF
	require 'inc/customPostType.php';

}


// Désactive les champs ACF de produits si l'option n'est pas active depuis la page options du backoffice
add_filter('acf/prepare_field/name=product_price', 'disable_acf_prepare_field');
add_filter('acf/prepare_field/name=product_promotion', 'disable_acf_prepare_field');
add_filter('acf/prepare_field/name=product_link', 'disable_acf_prepare_field');
add_filter('acf/prepare_field/name=livraison_info', 'disable_acf_prepare_field');
add_filter('acf/prepare_field/name=product_dimensions', 'disable_acf_prepare_field');
add_filter('acf/prepare_field/name=product_poids', 'disable_acf_prepare_field');
add_filter('acf/prepare_field/name=livraison', 'disable_acf_prepare_field');


function disable_acf_prepare_field( $field ) {
	if(get_post_type_object(get_post_type())->type != 'produit'){
		$field['wrapper']['class'] = 'hidden';
	}
	return $field;
}



// CUSTOM WP_IS_MOBILE
function my_wp_is_mobile() {
    static $is_mobile;

    if ( isset($is_mobile) )
        return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
        $is_mobile = false;
    } elseif (
        strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $is_mobile = false;
    } else {
        $is_mobile = false;
    }

    return $is_mobile;
}



/* -------------------------------------------
	CUSTOM COLORS & Gutemberg custom class
--------------------------------------------- */
function socreativ_custom_color() {

	# get ACF Color
	$white = get_field('b&w_color', 'options')['white'];
	$black = get_field('b&w_color', 'options')['black'];

	$grey = get_field('grey_color', 'options')['grey'];
	$light_grey = get_field('grey_color', 'options')['light_grey'];
	$dark_grey = get_field('grey_color', 'options')['dark_grey'];

	$primary = get_field('main_color', 'options')['primary'];
	$primary_dark = get_field('main_color', 'options')['primary_dark'];
	$secondary = get_field('main_color', 'options')['secondary'];

	// Add custom color
	add_theme_support( 'editor-color-palette', array(
		array( 'name' => 'White', 'slug'  => 'white', 'color' => $white ),
		array( 'name' => 'Black', 'slug'  => 'black', 'color' => $black ),

		array( 'name' => 'Grey', 'slug'  => 'grey', 'color' => $grey ),
		array( 'name' => 'Light Grey', 'slug'  => 'light_grey', 'color' => $light_grey ),
		array( 'name' => 'Dark Grey', 'slug'  => 'dark_grey', 'color' => $dark_grey ),

		array( 'name' => 'Primary', 'slug'  => 'primary', 'color' => $primary ),
		array( 'name' => 'Primary Dark', 'slug'  => 'primary_dark', 'color' => $primary_dark ),
		array( 'name' => 'Secondary', 'slug'  => 'secondary', 'color' => $secondary ),
	));

	// Disable Gutenberg Custom Colors
	add_theme_support( 'disable-custom-colors' );
	// Disable Gutenberg Custom Gradients
	add_theme_support('disable-custom-gradients', true);
	add_theme_support('editor-gradient-presets', []);

}

socreativ_custom_color();

// CUSTOM GUTENBERG FONT SIZE
add_theme_support('editor-font-sizes',
array(
	array(
		'name'      => __( 'Big Title', 'socreativ-theme' ),
		'shortName' => __( 'N', 'socreativ-theme' ),
		'size'      => 70,
		'slug'      => 'big'
	),
	array(
		'name'      => __( 'Big text', 'socreativ-theme' ),
		'shortName' => __( 'F', 'socreativ-theme' ),
		'size'      => 30,
		'slug'      => 'bigtext'
	),
	array(
		'name'      => __( 'small text', 'socreativ-theme' ),
		'shortName' => __( 'A', 'socreativ-theme' ),
		'size'      => 16,
		'slug'      => 'small'
	)
)
);

// BUTTON STYLE
register_block_style(
'core/button',
array(
'name'  => 'light-link',
'label' => __( 'Light Link', 'wp-light-link' ),
)
);
// COLUMNS
register_block_style(
'core/columns',
array(
'name'  => 'columns-link',
'label' => __( 'Columns Pictos Link', 'wp-columns-link' ),
)
);
// COLUMN sqlite_fetch_single
register_block_style(
'core/column',
array(
'name'  => 'column-border',
'label' => __( 'Column with border', 'wp-column-border' ),
)
);
// GROUP
register_block_style(
'core/group',
  array(
	'name'  => 'container',
	'label' => __( 'group container on grid', 'wp-group-container' ),
 )
);
register_block_style(
'core/group',
  array(
	'name'  => 'container-wide',
	'label' => __( 'group container on grid wide', 'wp-group-container-wide' ),
 )
);
register_block_style(
'core/group',
  array(
	'name'  => 'container-small',
	'label' => __( 'group container small center', 'wp-group-small-container' ),
 )
);
register_block_style(
'core/group',
  array(
	'name'  => 'container-half-right',
	'label' => __( 'group container half right', 'wp-group-half-right-container' ),
 )
);
register_block_style(
'core/group',
  array(
	'name'  => 'container-half-left',
	'label' => __( 'group container half left', 'wp-group-half-left-container' ),
 )
);
register_block_style(
'core/image',
  array(
	'name'  => 'rounded-image',
	'label' => __( 'rounded image', 'rounded-image' ),
 )
);

/*********************************************************************
                        PLUGIN DEPENDENCIES
 ********************************************************************/

require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'socreativ_register_required_plugins' );


function socreativ_register_required_plugins() {

	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'Advanced Custom Fields Pro', // The plugin name.
			'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
		),

		array(
			'name'        => 'iThemesecurity',
			'slug'        => 'better-wp-security',
			'force_activation'   => true
		)
	);

	if(get_field('isShop', 'options'))
		$plugins[] = array(
			'name'		=> 'WooCommerce',
			'slug'		=> 'woocommerce',
			'force_activation'	=> true
		);

	$config = array(
		'id'           => 'theme_by_socreativ',    		// Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      		// Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', 		// Menu slug.
		'parent_slug'  => 'themes.php',            		// Parent menu slug.
		'capability'   => 'edit_theme_options',    		// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    		// Show admin notices or not.
		'dismissable'  => false,                   		// If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      		// If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   		// Automatically activate plugins after installation or not.
	);

	tgmpa( $plugins, $config );

}

add_filter( 'plugin_action_links', 'blacklist_plugins', 10, 4 );
function blacklist_plugins( $actions, $plugin_file, $plugin_data, $context ) {
 
    if ( array_key_exists( 'activate', $actions ) && in_array( $plugin_file, array(
		'elementor/elementor.php',
		'divi-builder/divi-builder.php',
		'wp-bakery/wp-bakery.php',
		'visualcomposer/plugin-wordpress.php'
	)))
        unset( $actions['activate'] );
 
    return $actions;
}

function deactive_blacklist_plugin(){

	if(!function_exists('is_plugin_active')){
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	}


	$blacklist_plugins = array(
		'elementor/elementor.php',
		'divi-builder/divi-builder.php',
		'wp-bakery/wp-bakery.php',
		'visualcomposer/plugin-wordpress.php'
	);

	foreach ($blacklist_plugins as $index => $plugin) {
		if(is_plugin_active($plugin)){
			deactivate_plugins($plugin);
		}
	}

}
deactive_blacklist_plugin();

/* -------------------------------------------
				Woocommerce support
--------------------------------------------- */

// Admin styles & scripts
function load_admin_assets(){
	wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/assets/css/admin.css' );
	wp_enqueue_script( 'admin-scripts', get_template_directory_uri() . '/js/admin.js', array(), '1.0.0', true );
	
	$user = wp_get_current_user();
	$isAdmin = in_array( 'administrator', (array) $user->roles );

	wp_localize_script( 'admin-scripts', 'admin_data', array(
		'isAdmin' => $isAdmin
	));
}

add_action( 'admin_enqueue_scripts', 'load_admin_assets');

function checkWooCommerceStatus(){
	if(!function_exists('is_plugin_active'))
		require_once ABSPATH . 'wp-admin/includes/plugin.php';

	if(is_plugin_active('woocommerce/woocommerce.php') && !get_field('isShop', 'option')){
		deactivate_plugins('woocommerce/woocommerce.php');
	}

	if(is_plugin_active('woocommerce/woocommerce.php')){
		require_once get_template_directory() . '/inc/woocommerce-support.php';
	}
	
	add_filter( 'plugin_action_links', 'removeWooCommerceActivation', 10, 4 );
}

function removeWooCommerceActivation( $actions, $plugin_file, $plugin_data, $context ){
	if(array_key_exists('activate', $actions) 
		&& !get_field('isShop', 'options') 
		&& $plugin_file == 'woocommerce/woocommerce.php'){
			unset($actions['activate']);
		}
	return $actions;
}

checkWooCommerceStatus();


/* -------------------------------------------
				Custom functions
--------------------------------------------- */

function return_terms_index($taxo, $class = '', $subclass = '') {
	$taxonomies = array( 
	  $taxo,
	);
  
	$args = array(
	  'orderby'           => 'name', 
	  'order'             => 'ASC',
	  'hide_empty'        => false, 
	  'fields'            => 'all',
	  'parent'            => 0,
	  'hierarchical'      => true,
	  'child_of'          => 0,
	  'pad_counts'        => false,
	  'cache_domain'      => 'core'    
	);
  
	$terms = get_terms($taxonomies, $args);
  
	$return = '<ul class="'.$class.'">'; 
  
	  foreach ( $terms as $term ) {
  
		// return terms
		$return .= sprintf(
		  '<li id="category-%1$s" class="toggle main-cat">%2$s',       
		  $term->term_id, $term->name);
  
		  $subterms = get_terms($taxonomies, array(
			'parent'   => $term->term_id,
			'hide_empty' => false
			));
  
		  $return .= '<ul class="'.$subclass.'">';
  
		  foreach ( $subterms as $subterm ) {
  			$return .= sprintf(
			'<li id="category-%1$s" class="toggle sub-cat"><a href="%2$s" class="sub-cat-link">%3$s',       
			$subterm->term_id, get_term_link($subterm->term_id), $subterm->name);
			$return .= '</a></li>';
		  }

		  $return .= '</ul>';
		$return .= '</li>';
	  }
  
	$return .= '<li class="main-cat"><a href="'.get_post_type_archive_link(get_post_type()).'">Toutes</a></li></ul>';
  
  return $return;
  }


  # Add taxonomy to search engine
  function custom_search_where($where){ 
	global $wpdb;
	if (is_search() && get_search_query())
	  $where .= "OR ((t.name LIKE '%".get_search_query()."%' OR t.slug LIKE '%".get_search_query()."%') AND {$wpdb->posts}.post_status = 'publish')";
	return $where;
  }
  
  # Add taxonomy relationship to search engine
  function custom_search_join($join){
	global $wpdb;
	if (is_search()&& get_search_query())
	  $join .= "LEFT JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id INNER JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN {$wpdb->terms} t ON t.term_id = tt.term_id";
	return $join;
  }
  
  # Add taxonomy relationship groupby to search engine
  function custom_search_groupby($groupby){
	global $wpdb;
	// we need to group on post ID
	$groupby_id = "{$wpdb->posts}.ID";
	if(!is_search() || strpos($groupby, $groupby_id) !== false || !get_search_query()) return $groupby;
	// groupby was empty, use ours
	if(!strlen(trim($groupby))) return $groupby_id;
	// wasn't empty, append ours
	return $groupby.", ".$groupby_id;
  }
  
  add_filter('posts_where','custom_search_where');
  add_filter('posts_join', 'custom_search_join');
  add_filter('posts_groupby', 'custom_search_groupby');


  // Multi site comment / pings disabled
  add_filter( 'pings_open', '__return_false', 10, 2 );
  add_filter( 'comments_open', '__return_false', 10, 2 );


