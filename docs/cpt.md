# Custom Post Type & Taxonomy

## Custom Post Type

Le thème possède un custom post type déjà déclaré en [php](../inc/customPostType.php). Le slug, le nom ainsi que l'icône sont dynamiquement récupérer depuis la [page option](./options.md#markdown-header-custom-post-type). 
<br>
L'options ***Produits*** permet de modifier le comportement du custom post type et d'avoir des paramètres de personnalisation supplémentaire tel que le prix, poids, mesures, lien de paiements (Stripe / Paypal) et information additionnels pour simuler une boutique. 

Utile pour les personnes souhaitant une présentation de boutique sans paiements sur le site mais via un tiers (Stripe etc..) pour un prix plus bas qu'une boutique Woocommerce.


## Taxonomy

Une seul taxonomy associé au CPT à également été déclaré dans le thème. Son slug se base sur celui du CPT suivant ce format: 

    cpt_slug_category

Si le slug du custom post type est changé, alors celui de la taxonomy le seras aussi et les termes devront donc être réintégré.

Seul le nom d'affichage de la taxonomy peut être modifier depuis le backoffice.

La taxonomy possède une option ***Single*** permettant d'ajouter 2 nouveaux champs de personnalisation (Texte + Image) permettant une présentation du terme, comme pour un artiste (Cf. [galerie falck](https://galerie-falck.com/oeuvre_category/robert-combas/)). 