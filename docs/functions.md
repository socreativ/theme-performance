# Fonctions particulière


## PHP

### Dépendance plugins

Il y a plusieurs [fonction](../functions.php#functions.php-543) utilisé dans les autres thèmes Sõcreativ' permettant de gérer les dépendances de plugins. Cela permet de s'assurer que certains plugin vitaux sois activé avec le thèmes (ACF) et de désactiver / interdire l'activation d'autre (Visual Composer, WP Bakery etc...).  <br>
Le but étant de s'assurer que le site ne sois pas cassé à cause d'une fausse manipulation dans l'espace extension. 

### Woocommerce support

Il y a 2 [fonctions](../functions.php#functions.php-632) crée dans le but de verrouiller l'activation / désactivation du plugin Woocommerce à un utilisateur n'étant pas *Super Admin*, et de forcer l'activation depuis la page options.<br>
Le but étant d'être sur qu'un client ne désactive pas le plugin sur son site dépendant du plugin et inversement qu'il ne l'active pas sur une site n'étant pas été vendu pour.

### Divers

Il y a de nombreuses petites fonctions personalisé dans le [functions.php](../../functions.php), comme l'ajout de la taxonomy au système de recherche natif Wordpress qui sont directement commenté dans le code.

## Javascript

### Classe z-index

Pour faciliter la gestion des z-index directement depuis Gutenberg, il existe une [fonction](../../assets/js/script.js#script.js-271) qui assigne une [classe](../../assets/css/main.css#main.css-51) css avec une valeur dépendant de la classe. 
Ex: `z-index-15` donnera une classe z-index-x avec l'attribut `--index` à 15. 
