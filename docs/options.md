# Page options

### Principe

La majorité des options de personnalisation du site se trouvent sur cette page options utilisant ACF. C'est depuis cette page que les intégrateurs peuvent paramétrer et intégré les informations vital du site. (Titre, logo, info clients, style, couleur, woocommerce, custom post type, etc...).

Pour rajouter des options, il suffit simplement dans ACF d'ajouter des champs dans la *page option* puis de correctement conditionner en PHP.

### Sommaire

 1. [Options générales](#options-generales)
 2. [Coordonnées](#coordonnnees)
 3. [Logo](#logo)
 4. [Footer](#footer)
 5. [Google Font](#google-font)
 6. [Scripts additionnels](#scripts-additionnels)
 7. [Couleurs et styles](#couleurs-et-styles)
 8. [Page 404 & Buildmode](#page-404-&-buildmode)
 9. [Custom Post Type](#custom-post-type)
 10. [Woocommerce](#woocommerce)

## Options générales

<img src="https://bitbucket.org/socreativ/theme-performance/raw/0e87632f08fa48b8b52a23edfa0508a5c1facfbd/docs/images/options_general.png" />

- Titre du site afficher dans le footer: *name*
- Description du site afficher dans le footer: *desc*

## Coordonnées
<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_coordonnes.png">

Différentes coordonnées affichées dans le footer:

 - Numéro de téléphone: *contact_tel*
 -  Adresse: *adresse_contact*
 - Adresse email: *contact_mail*
 - Horaires: *horaires*

## Logo

<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_logo.png">

Logo afficher dans le header et footer
- Groupe ACF logo: *logo*
- Format (svg ou png/jpg): *isSVG*
- Logo svg: *svg*
- Logo sombre: *dark*
- Logo clair: *light*

## Footer

<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_footer.png" />

Paragraphe additionnel sous le footer.
 - Paragraphe: *last_p*

## Google font

<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_fonts.png">

Gestion des fonts google. Lien google fonts + nom de la font assigner dans la variables css `--font`

 - Lien Google font (Balise html link): *google_font*
 - Nom de la font utiliser pour la variable: *font_name*

## Scripts additionnels

<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_scripts.png">

Champs de texte permettant d'ajouter des scripts, balises lien dans le header ou footer (ex: analytics)

 - Emplacement header: *head_scripts*
 - Emplacement footer: *footer_scripts*

## Couleurs et styles

<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_colors.png">

Customization des couleurs du sites et styles alternatifs. Influe la majorités des blocks du site (Header, footer, block gutenberg).<br/>
Des possibilités de styles supplémentaires peuvent y être ajouté. <br/>

Groupes ACF: 
 - Noir et blanc: *b&w_color*
 - Nuance de gris: *grey_color*
 - Couleur principales: *main_color*
 - Styles alternatifs: *other_styles*

## Page 404 & Buildmode

<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_404.png">

Gestion des pages 404 et du buildmode. Permet de gérer les messages et le background de la page 404 et l'option buildmode.

 - Page 404: *404*
 - Buildmode: *buildmode*

## Custom Post Type

<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_cpt.png">

Permet de crée automatique un custom post type avec une taxonomy associé. <br>
Le type **produit** est une alternative basique à woocommerce, permettant d'ajouter des informations additionnel (Prix, poids, moyen de livraisons, lien stripe etc..). Cette solution s'adresse à une présentation de produits / service n'ayant pas pour vocation à être vendu depuis le site. (Ex: tatoueur, coiffeur, artisan etc..)
<br>
Le type de taxonomie *single* permet d'ajouter des champs texte et image au termes de la taxonomy qui seront afficher sur la page de celui ci.

 - Type produits: *type*
 - Label, nom, slug relatif au custom post type: *cpt* 
 - Nom de la taxonomy (Slug fixe et permanent): *taxo*
 - Comportement taxonomy: *taxo_type*

## Woocommerce

<img src="https://bitbucket.org/socreativ/theme-performance/raw/b3f6b77e0e1ad2ce63daebd11befde7e138ade47/docs/images/options_woocommerce.png" /> 

Permet d'activer le plugin Woocommerce. Cette options est modifiable seulement à l'aide d'un utilisateur administrateur.<br> Woocommerce ne pourra pas être activer à la main si cette option est décoché, de même manière qu'il ne pourra pas être désactiver si elle est coché.

Si des options de personnalisations supplémentaire relative à Woocommerce doivent être ajouter, elle pourront être ajouter dans cette section.