# Multisite & mise à jour

## Multisite - Badge-vert

Le thème structure est actuellement déployé sur le multisite [badge-vert](https://badge-vert.com/). Tout les sites client avec l'offre ***performance*** sont sur le même réseau et sont basé sur ce thème, ce qui signifie qu'a la mise à jour du thème sur *badge-vert* tout les sites en bénéficie. 

## Déploiement

La branche master déploie automatiquement les modifications sur l'environnement de production multisite de Sõcreativ' étant le seul site (multisite) utilisant actuellement le thème.<br>
Dans le cas d'un déploiement sur un nouveau site, il faut ajouter une ligne dans le *job* actuel pour upload les fichiers sur la nouvelle cible: 

    - git ftp push --user $userVar --passwd $pwdVar serverPath
