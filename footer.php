<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme-by-socreativ
 */
?>
	<?php if(get_field('isModal')): ?>
		<?php if (get_field('modal_type')): $content = get_field('notif_content'); ?>
			<?php if(!my_wp_is_mobile()): ?>
			<div class="notification anim-300 d-none d-md-block closed" data-delay="<?=  get_field('delay'); ?>">
				<button type="button" class="close-modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<div class="d-flex">
					<div>
						<p><?=  $content['texte'] ?></p>
					</div>
				</div>
				<div class="d-flex justify-center">
					<a class="custom-btn" target="<?=  $content['bouton']['target'] ?>" href="<?=  $content['bouton']['url'] ?>"><?=  $content['bouton']['title'] ?></a>
				</div>
			</div>
			<?php endif; ?>
		<?php else: $content = get_field('modal_content'); ?>
			<div class="modal custom-modal closed" tabindex="-1" role="dialog" data-delay="<?=  get_field('delay'); ?>">
				<div class="modal-dialog <?php if($content['visuel'] && !my_wp_is_mobile()) echo 'modal-big' ?> modal-dialog-centered" role="document">
					<?php if($content['visuel'] && !my_wp_is_mobile()): ?>
						<img class="modal-visuel" src="<?=  $content['visuel']['sizes']['medium'] ?>" alt="<?=  $content['visuel']['alt'] ?>" height="<?=  $content['visuel']['height'] ?>" width="<?=  $content['visuel']['width'] ?>">
 					<?php endif; ?>
					<div class="modal-content p-3">
						<div class="modal-header d-flex justify-content-between">
							<h2 class="modal-title"><?=  $content['titre']; ?></h2>
							<button type="button" class="close-modal" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p><?=  $content['texte']; ?></p>
						</div>
						<div class="modal-footer">
							<?php if(isset($content['button'])): ?>
								<a class="custom-btn" target="<?=  $content['bouton']['target'] ?>" href="<?=  $content['bouton']['url'] ?>"><?=  $content['bouton']['title'] ?></a>
							<?php else: ?>
								<button type="button" class="close-modal custom-btn" data-dismiss="modal" aria-label="Close">Fermer</button>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	<!-- BEGIN FOOTER -->
	<footer id="colophon" class="site-footer <?php if(isset($args['css'])) echo $args['css']; ?>">

	<div class="footer-container">
		<div class="footer-banner">
			<div class="logo-footer container">
				<div class="row">
					<div class="col-12 col-lg-3 p-0 site-title">
						<?php if(get_field('name', 'options')): ?>
							<h2 class="w-100 text-center"><?=  get_field('name', 'options'); ?></h2>
						<?php endif; ?>
					</div>
					<div class="col-12 col-lg-6 text-center text-lg-left justify-content-center justify-content-lg-start pl-0 pl-lg-5 site-desc">
						<?php if(get_field('desc', 'options')): ?>
							<p><?=  get_field('desc', 'options'); ?></p>
						<?php endif; ?>
					</div>
					<div class="col-12 col-lg-3 RS-footer text-center text-lg-left justify-content-center justify-content-lg-end  my-5 my-lg-0">
					<a href="#contact" class="rounded-btn mr-4 footer-devis">Contact</a>

						<?php
							if(!get_field('is_custom_menu_footer')):
								wp_nav_menu(
									array(
										'theme_location' => 'RS',
										'container_class' => '',
										'container' => 'ul',
										'menu_id' => 'rs-menu',
										'menu_class' => 'lh-15 ml-0 pl-0 list-unstyled',
									)
								);
							endif;
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row py-4">
				<div class="col-12 col-md-6 col-lg-4 d-flex flex-column mb-2 mb-md-0">
					<?php
					if(!get_field('is_custom_menu_footer')):
						wp_nav_menu(
							array(
								'theme_location' => 'footer',
								'container_class' => '',
								'container' => 'ul',
								'menu_id' => 'footer-menu',
								'menu_class' => 'lh-15 ml-0 pl-0 list-unstyled',
							)
						);
					else: ?>
						<ul id="footer-menu" class="lh-15 fs-18 ml-0 pl-0 list-unstyled">
					<?php if(have_rows('custom_menu_footer')):
							while (have_rows('custom_menu_footer')):
								the_row();
								$link = get_sub_field('custom_menu_footer_link'); ?>
								<li class="menu-item"><a href="<?=  $link['url']; ?>"><?=  $link['title']; ?> </a></li>
					<?php	endwhile;
						endif; ?>
						</ul>
					<?php endif; ?>
				</div>
				<?php if ( have_rows('adresse_contact','option') ): $i=0; ?>
					<?php while ( have_rows('adresse_contact','option') ) : the_row(); ?>
						<div class="col-12 col-md-6 col-lg-4 d-flex flex-column mb-2 mb-md-0 text-center text-md-left">

							<!-- ADRESSE -->
							<?php if (get_sub_field('titre')): ?>
								<h2 class="fw-700 mb-3 address-title"><?php the_sub_field('titre'); ?></h2>
							<?php endif; ?>
							<?php
								$link = get_sub_field('adresse') ? get_sub_field('adresse') : ['url' => '#', 'title' => '', 'target' => '_self'];
								$link_url = $link['url'] ? $link['url'] : '#';
								$link_title = $link['title'] ? $link['title'] : '';
								$link_target = $link['target'] ? $link['target'] : '_self';
							?>
									<a class="d-flex flex-column text-white" href="<?=  esc_url( $link_url ); ?>" target="<?=  esc_attr( $link_target ); ?>" rel="nofollow">
											<?php the_sub_field('adresse_detaillee'); ?>
									</a>
									<hr>
									<a class="anim-300 adresse-link d-flex flex-column align-items-start text-white fs-12" href="<?=  esc_url( $link_url ); ?>" target="<?=  esc_attr( $link_target ); ?>" rel="nofollow">
										<span class="anim-300">
											<?=  esc_html( $link_title ); ?>
										</span>
									</a>

						<!-- END ADRESSE -->
						</div>
					<?php $i++; endwhile; ?>
				<?php endif; ?>
				<?php if ( have_rows('contact_tel','option') ): ?>
					<div class="col-12 col-md-6 col-lg-4 d-flex flex-column mb-2 mb-md-0 text-center text-md-left">
						<!-- TEL -->
								<?php while ( have_rows('contact_tel','option') ) : the_row(); ?>
									<?php if (get_sub_field('titre')): ?>
										<h4 class="fs-16 fw-700 mb-3"><?php the_sub_field('titre'); ?></h4>
									<?php endif; ?>
									<?php
									$link = get_sub_field('tel');
									if( $link ):
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
											?>
											<a class="d-block text-white fs-16 fw-700" href="<?=  esc_url( $link_url ); ?>" target="<?=  esc_attr( $link_target ); ?>" rel="nofollow">
												<?=  esc_html( $link_title ); ?>
											</a>
									<?php endif; ?>
								<?php endwhile; ?>
						<?php endif; ?>
						<!-- MAIL -->
						<?php if ( have_rows('contact_mail','option') ): ?>
								<?php while ( have_rows('contact_mail','option') ) : the_row(); ?>
									<?php if (get_sub_field('titre')): ?>
										<h4 class="fs-16 fw-700 mb-3"><?php the_sub_field('titre'); ?></h4>
									<?php endif; ?>
									<?php
									$link = get_sub_field('adresse');
									if( $link ):
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
											?>
											<a class="d-block text-white mt-1 fs-16 fw-700" href="<?=  esc_url( $link_url ); ?>" target="<?=  esc_attr( $link_target ); ?>" rel="nofollow">
												<?=  esc_html( $link_title ); ?>
											</a>
									<?php endif; ?>
								<?php endwhile; ?>
						<?php endif; ?>
						<?php if (get_field('horaires','option')): ?>
							<hr>
							<div class="">
								<?php the_field( 'horaires','option' ); ?>
							</div>
					</div>
				<?php endif; ?>


			</div>
		</div>
	</div>
	<div class="sub-footer">
		<div class="container">
			<?php if(get_field('last_p', 'options') != '' || get_field('custom_text_footer') != ''){ ?>
			<div class="fs-14 py-5">
				<?php
				if(!get_field('is_custom_text_footer')){
				 echo get_field('last_p', 'options');
				}
				else{
					echo get_field('custom_text_footer');
				} ?>
			</div>
			<?php } ?>
		</div>
		<div class="sub-footer-legacy text-center fs-12 py-2">
			<?php dynamic_sidebar( 'sub-footer-widget' ); ?>
		</div>
	</div>
	</footer>
	<!-- END FOOTER -->
	<?php if (get_field('page_wip') && !is_user_logged_in()) : ?>
	<!-- BUILDING MODE -->
	<div class="building-mode">
		<img class="building-img" src="<?=  get_field('buildmode', 'options')['bkg']['url']; ?>" alt="<?=  get_field('buildmode', 'options')['bkg']['alt']; ?>">
		<div class="greyscale"></div>
		<div class="text-building-mode no-bkg">
			<div class="mt-5vh text-center op-0">
				<h2 class="text-white fs-48 fw-700"><?=  get_field('buildmode', 'options')['titre']; ?></h2>
				<h3 class="text-white fs-22 mb-5vh"><?php the_title(); ?></h3>
				<p class="text-white fs-21 mb-5vh"><?=  get_field('buildmode', 'options')['texte']; ?><p>
				<a class="build-link anim-300" href="<?=  get_site_url(); ?>">
					Retour à l'accueil
				</a>
			</div>
		</div>
	</div>
		<style media="screen">
			body.building-mode{
				height: 100vh;
				overflow: hidden;
			}
			.building-mode{
				background-color: #000;
				width: 100vw;
				height: 100vh;
				position: fixed;
				z-index: 99999;
				top: 0;
				left: 0;
			}
			.building-img{
				width: 100%;
				height: 100%;
			}
			.build-link{
				color: #fff;
			}
			.build-link:hover{
				color: #fff;
			}
			.text-building-mode {
		position: fixed;
		z-index: 9;
		top: 50%;
		transform: translate(0%, -50%);
		text-align: center;
		color: #fff;
		width: 100%;
		height: 25%;
			}
		</style>
	<!-- END BUILDING MODE -->
	<?php endif; ?>
<?php wp_footer(); ?>
</body>
<?=  get_field('footer_scripts', 'options') ?>
</html>
