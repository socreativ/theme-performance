<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme-by-socreativ
 */

 if(get_field('isShop', 'options')) global $woocommerce;

 # get ACF Color
 $white = get_field('b&w_color', 'options')['white'] ?: "#000";
 $black = get_field('b&w_color', 'options')['black'] ?: "#FFF";
 $grey = get_field('grey_color', 'options')['grey'] ?: "#161616";
 $light_grey = get_field('grey_color', 'options')['light_grey'] ?: "#CCC";
 $dark_grey = get_field('grey_color', 'options')['dark_grey'] ?: '#666';
 $primary = get_field('main_color', 'options')['primary'] ?: "#090";
 $primary_dark = get_field('main_color', 'options')['primary_dark'] ?: "#0F0";
 $secondary = get_field('main_color', 'options')['secondary'] ?: "#F00";
 $logo_bkg = get_field('b&w_color', 'options')['logo_bkg_color'] ?: "#FFF";
 $font = get_field('fontname', 'options');

 $other_css = 'classic-header ';
 $other_css .= get_field('other_styles', 'options')['faq_style'] ? 'faq-light ' : '';
 if(count($args) != 0) $other_css .= $args['css'];
 
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<?php the_field( "google_font","option" ); ?>
	<?php wp_head(); ?>
	<style>
		:root {
		/* déclaration variable couleurs */
		--white: <?=  $white ?>;
		--black: <?=  $black ?>;
		--grey: <?=  $grey ?>;
		--dark-grey: <?=  $dark_grey ?>;
		--light-grey: <?=  $light_grey ?>;
		--dark-primary: <?=  $primary_dark ?>;
		--primary: <?=  $primary ?>;
		--secondary: <?=  $secondary ?>;
		--font: '<?=  $font; ?>';
	}
	</style>
	<?=  get_field('head_scripts', 'options') ?>
</head>
<!-- BEGIN PAGE -->
<body <?php body_class($other_css); ?>>
<?php wp_body_open(); ?>

	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'theme-by-socreativ' ); ?></a>

	<!-- BEGIN HEADER -->
	<header id="masthead" class="anim-300 site-header <?= get_field('other_styles', 'options')['menu_style']; ?>">

		<div class="logo anim-300" style="--logo-bkg: <?= $logo_bkg ?>">
			<?php if(get_field('logo_isSVG', 'options')): ?>
				<a href="<?= get_home_url() ?>"> <?=  get_field('logo_svg', 'options'); ?> </a>
			<?php else: ?>
				<a href="<?= get_home_url() ?>">

					<?php if(get_field('other_styles', 'options')['menu_style'] == 'header-light'): ?>
							<img class="logo-l" src="<?= get_field('logo', 'options')['light']['url']; ?>" alt="<?= get_field('logo', 'options')['light']['alt']; ?>">
							<img class="logo-d" src="<?= get_field('logo', 'options')['dark']['url']; ?>" alt="<?= get_field('logo', 'options')['light']['alt']; ?>">
					<?php else: ?>
						<?php if(my_wp_is_mobile()): ?>
							<img src="<?=  get_field('logo', 'options')['dark']['sizes']['medium']; ?>" height="<?=  get_field('logo_dark', 'options')['height'] ?>" width="<?=  get_field('logo_dark', 'options')['width'] ?>" alt="<?=  get_field('logo_dark', 'options')['alt'] ?>" >
						<?php else: ?>
							<img src="<?=  get_field('logo', 'options')['dark']['url']; ?>" height="<?=  get_field('logo_dark', 'options')['height'] ?>" width="<?=  get_field('logo_dark', 'options')['width'] ?>" alt="<?=  get_field('logo_dark', 'options')['alt'] ?>" >
						<?php endif; ?>
					<?php endif; ?>

				</a>
			<?php endif; ?>
		</div>

		
		<?php if(!my_wp_is_mobile()): ?>
		<nav class="global-menu">
			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'main-menu',
						'container_id' => 'main-menu-container',
						'container_class' => 'main-menu-container anim-300 d-flex',
						'menu_id' => 'main-menu',
						'menu_class' => 'main-menu anim-300 d-flex list-unstyled text-white w-100',
					)
				);
			?>
		</nav>
				<?php if(get_field('isShop', 'options')): ?>
						<?php if(!is_cart() && !is_checkout()): ?>
							<div class="cart-summary">
								<a href="<?= wc_get_cart_url() ?>">
									<div class="position-relative">
										<svg xmlns="http://www.w3.org/2000/svg" height="24" width="24" viewBox="0 0 64 64"><title>cart</title><g fill="#ffffff" class="nc-icon-wrapper"><circle data-color="color-2" cx="10.5" cy="54.5" r="6.5"></circle><circle data-color="color-2" cx="53.5" cy="54.5" r="6.5"></circle><path d="M60,42H5.7l3.889-7H48a1,1,0,0,0,.948-.684l8-24A1,1,0,0,0,56,9H9.469l-4.7-5.641A1,1,0,1,0,3.231,4.641L8,10.362V33.741L3.126,42.515A1,1,0,0,0,4,44H60a1,1,0,0,0,0-2Z" fill="#ffffff"></path></g></svg>								
										<span id="cart_count"><?= $woocommerce->cart->get_cart_contents_count() ?></span>	
									</div>
								</a>
								<?php if($woocommerce->cart->get_cart_contents_count() !== 0): ?>
									<div class="cart-wrapper">
										<?php $products = $woocommerce->cart->cart_contents; ?>
										<div class="cart-wrapper-inner">
											<?php foreach($products as $key => $product): ?>
												<?php $product = $product['data']; ?>
												<div class="d-flex">
													<img src="<?= wp_get_attachment_image_url($product->get_image_id(), 'thumbnail') ?>" />
													<p class="cart-name-head">
														<a href="<?= get_site_url(); ?>/?p=<?= $product->get_id(); ?>">
															<?= $product->get_name(); ?>
														</a>
													</p>
													<p class="price-cart-head"><strong><?= $product->get_price() . get_woocommerce_currency_symbol() ?></strong></p>
													<a class="complete-from-cart" href="<?= wc_get_cart_url() ?>">✓</a>
													<a class="remove-from-cart" href="<?= wc_get_cart_remove_url($key) ?>">X</a>
												</div>
											<?php endforeach; ?>
										</div>
									</div>
								<?php endif; ?>
							</div>
						<?php endif; ?>
				<?php endif; ?>
		<?php else: ?>
				<nav class="mobile-main-menu anim-800">
					<div class="burger-menu anim-300">
						<div class="menu-toggle d-block d-lg-none position-relative">
							<!-- <p class="m-0">MENU</p> -->
							<span class="hamb-line anim-300"></span>
							<span class="hamb-line anim-300"></span>
							<span class="hamb-line anim-300 m-0"></span>
							<span class="crose-line anim-300"></span>
							<span class="crose-line crose-line2 anim-300"></span>
						</div>
					</div>
					<div class="mobile-menu-container">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'main-menu',
									'container_class' => 'mobile-container-menu anim-800',
									'menu_id' => 'mobile-menu',
									'menu_class' => 'mobile-menu anim-300 d-flex list-unstyled text-white w-100',
								)
							);
						?>
					<div class="mobile-social">
					<?php
							if(!get_field('is_custom_menu_footer')):
								wp_nav_menu(
									array(
										'theme_location' => 'RS',
										'container_class' => '',
										'container' => 'ul',
										'menu_id' => 'rs-menu-mobile',
										'menu_class' => 'lh-15 ml-0 pl-0 list-unstyled',
									)
								);
							endif;
						?>
					</div>
				</div>
			</nav>
		<?php endif; ?>
		
	</header>
	<!-- END HEADER -->
