<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package theme-by-socreativ
 */

 

?>


<!doctype html>

<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="preconnect" href="https://fonts.gstatic.com">

	<?php the_field( "google_font","option" ); ?>
	<?php wp_head(); ?>

	<style>
		:root {
		/* déclaration variable couleurs */
		--white: <?=  $white ?>;
		--black: <?=  $black ?>;

		--grey: <?=  $grey ?>;
		--dark-grey: <?=  $dark_grey ?>;
		--light-grey: <?=  $light_grey ?>;

		--dark-primary: <?=  $primary_dark ?>;
		--primary: <?=  $primary ?>;
		--secondary: <?=  $secondary ?>;
	}
	</style>

	<?=  get_field('head_scripts', 'options') ?>

</head>
<body>
	<main id="primary" class="site-main">

		<section class="error-404 not-found">
		<div class="page-404">
			<img class="img-404" src="<?=  get_field('404', 'options')['bkg']['url']; ?>" alt="<?=  get_field('404', 'options')['bkg']['alt']; ?>">
			<div class="greyscale"></div>
			<div class="text-404 no-bkg">
				<div class="mt-5vh text-center op-0">
					<h2 class="text-white fs-48 fw-700"><?=  get_field('404', 'options')['titre']; ?></h2>
					<h3 class="text-white fs-22 mb-5vh"><?php the_title(); ?></h3>
					<p class="text-white fs-21 mb-5vh"><?=  get_field('404', 'options')['texte']; ?><p>
					<a class="link-404 anim-300" href="<?=  get_site_url(); ?>">
						Retour à l'accueil
					</a>
				</div>
			</div>
		</div>	
		</section><!-- .error-404 -->

	</main>

</body>
	<style>
		body{
			height: 100vh;
			width: 100vw;
			overflow: hidden;
		}
		.error-404{
			display: flex;
			align-items: center;
			justify-content:center;
			width: 100vw;
			height: 100vh;
		}
		.page-404{
			background-color: #000;
		}
		.img-404{
			width: 100vw;
			height: 100vh;
			object-fit: cover;
			position: relative;
			z-index: 2;
		}
		.link-404{
			color: #fff;
		}
		.link-404:hover{
			color: #fff;
		}
		.text-404 {
			position: fixed;
			z-index: 9;
			top: 50%;
			transform: translate(0%, -50%);
			text-align: center;
			color: #fff;
			width: 100%;
			height: 25%;
		}
	</style>
</html>


<?php

