![Bitbucket open pull requests](https://img.shields.io/bitbucket/pr/socreativ/theme-performance)
![](https://img.shields.io/badge/PHP-7.4.29-informational)
# Thème performance - Sõcreativ' ©️

<a align="center"><img src="https://bitbucket.org/account/user/socreativ/projects/ST/avatar/32?ts=1652431584" height="100"/></a>
<p align=center>Thème performance customisable de l'agence Sõcreativ'. <br>
<a href="https://badge-vert.com/" target="_blank">Multisite Badge-vert</a>	 </p>


## Principe

Le principe du thème est basé sur la construction rapide de site grâce à une page d'options et d'une multitude de blocks gutenberg customisable (contenu, couleurs, options...) permettant la construction de page et la mise en ligne d'un site très rapidement sans développement.

## Documentation

- <a href="./docs/options.md">Page options</a>
- <a href="./docs/cpt.md">Custom Post type & Taxonomy</a>
- <a href="./docs/multisite.md">Multisite & mise à jour</a>
- <a href="./docs/functions.md">Fonctions particulières</a>