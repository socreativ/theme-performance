<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package theme-by-socreativ
 */

get_header(null, array('css' => 'f-sticky sticky'));
?>

	<main id="primary" class="site-main has-black-backraground-color">

		<?php
		while ( have_posts() ) :

			the_post();

			get_template_part( 'template-parts/content-cpt');

			

		endwhile; // End of the loop.
		?>

	</main>

<?php
get_footer();
