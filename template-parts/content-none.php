<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

?>

<section class="no-results not-found">
	<div class="search-header">
		<h1 class="page-title"><?php esc_html_e( 'Aucun résultat', 'theme-by-socreativ' ); ?></h1>
</div><!-- .page-header -->

	<div class="page-content position-relative has-white-color">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'theme-by-socreativ' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>

			<p><?php esc_html_e( 'Désolé, mais rien n\'a été trouvé avec vos termes. S\'il vous plait, essayez avec d\'autres mots clés.', 'theme-by-socreativ' ); ?></p>
			<?php
			get_search_form();

		else :
			?>

			<p><?php esc_html_e( 'Nous n\'avons pas trouvé ce que vous cherchiez. Cependant une rechercher pourrait aider.', 'theme-by-socreativ' ); ?></p>
			<?php
			get_search_form();

		endif;
		?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
