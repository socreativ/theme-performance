<div class="shop-product">
    <?php  get_template_part( 'template-parts/content-archive'); ?>

    <div class="d-flex jcc">
        <?php
            do_action( 'woocommerce_after_shop_loop_item_title' ); 
            do_action( 'woocommerce_after_shop_loop_item' );
        ?>
    </div>

</div>

