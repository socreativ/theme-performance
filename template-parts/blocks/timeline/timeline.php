<?php

/**
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Load values and assign defaults.
$nom = get_field('nom_modal');
$isVisible = get_field('timeline_visibility_mobile');

    if(wp_is_mobile() && !$isVisible){
        // Nothing here... Hide the timeline in mobile version if checkbox is checked
    }
    else{
        // In other cases ... Build timeline.
?>


<div id="<?=  $block['anchor'] ?>" class="timeline mt-sm-5vh sm-no-anim mt-10vh mb-5vh hello-X <?=  $block['className'] ?>">

    <div class="timeline-slider position-relative overflow-hidden mt-5vh h-400 h-sm-450">

        <div class="timeline-slider-container z-9 d-flex justify-content-between position-relative">
            <?php
            if (have_rows('dates')) :
                while (have_rows('dates')) : the_row();

                    // values
                    $date = get_sub_field('date');
                    $titre = get_sub_field('titre');
                    $description = get_sub_field('description');
                    $index = get_row_index();
            ?>

                    <div class="timeline-slide flex-column" >
                        <p class="slide-date text-blue fw-700 fs-20"><?=  $date ?></p>
                        <span class="slide-dot anim-300" data-slide="<?=  $index-1 ?>"></span>
                        <p class="cc mt-4">
                            <span class="m-0 fs-20 fw-700"><?=  $titre ?></span>    
                            <br>
                            <?=  $description ?>
                        </p>
                    </div>


            <?php
                endwhile;
            endif;
            ?>
        </div>


        <div id="timeline-prev" class="d-flex justify-content-center align-items-center position-absolute z-9 timeline-arrows">
            <img src="<?=  get_template_directory_uri() . '/assets/img/arrow.svg' ?>" alt="">
        </div>

        <div id="timeline-next" class="d-flex justify-content-center align-items-center position-absolute z-9 timeline-arrows">
            <img src="<?=  get_template_directory_uri() . '/assets/img/arrow.svg' ?>" alt="">
        </div>

        <div class="timeline-date-back position-absolute t-0 overflow-hidden d-flex">
            <div><?=  get_field('dates')[0]['date']; ?></div>
        </div>
        <span id="timeline-line" class="position-absolute"></span>


    </div>

</div>


    <?php } ?>