("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {



      /****************************************
                      TIMELINE
      ****************************************/

     $(".timeline").each(function () {
        const that = this;
        const tm = that.querySelector(".timeline-slider-container");
        const slides = tm.children;
        var slide = 0;
        const slideMax = slides.length - 1;
        const dateContainer = that.querySelector(".timeline-date-back");
        const sWidth = $(window).width()
      
        const prev = that.querySelector("#timeline-prev");
        const next = that.querySelector("#timeline-next");
        const line = that.querySelector("#timeline-line");
  
        if(sWidth > 768) ecart();
        slides[0].classList.add("slide-see");
  
        line.style.width =
          slides[0].offsetLeft + slides[0].offsetParent.offsetLeft + 16 + "px";
  
        newDate(dateContainer.querySelector("div").innerText, dateContainer);
  
        /* Events */
        next.addEventListener("click", function () {
          if (slide < slideMax) {
            // initTop(slide, slide + 1);
            slide += 1;
            toSlide(slides, line, slide);
          }
        });
        prev.addEventListener("click", function () {
          if (slide > 0) {
            // initTop(slide, slide - 1);
            slide -= 1;
            toSlide(slides, line, slide);
          }
        });
        that.querySelectorAll(".slide-dot").forEach(function (e) {
          e.addEventListener("click", function () {
            // initTop(slide, e.dataset.slide);
            slide = parseInt(e.dataset.slide);
            toSlide(slides, line, slide);
          });
        });
        window.addEventListener("resize", function () {
          if(sWidth > 768) ecart();
          toSlide(slides, line, slide);
        });
  
        function ecart() {
          for (let i = 0; i < slides.length; i++) {
            if (
              slides[i].offsetLeft +
                slides[i].querySelector(".cc").clientWidth >
              tm.clientWidth
            ) {
              slides[i].classList.add("slide-right");
            }
          }
        }
  
        function toSlide(slides, line, nb) {
          var lineWidth = parseInt(line.style.width, 10);
          var newLineWith =
            slides[nb].offsetLeft + slides[nb].offsetParent.offsetLeft + 16;
          var diff = newLineWith - lineWidth;
          const animF = 0.95;
  
          if (diff < 0) {
            diff *= -1;
          }
          line.style.transition = `${diff * animF}ms all ease-out`;
          // line.style.width = newLineWith + "px";
          line.style.width = newLineWith + "px";
  
          for (let i = 0; i < slides.length; i++) {
            slides[i].classList.remove("slide-see");
          }
          slides[nb].classList.add("slide-see");
  
          newDate(slides[nb].children[0].innerText);
        }
  
        function newDate(date) {
          date = date.split("");
  
          dateContainer.innerHTML = "";
          var newDate = document.createElement("div");
          date.forEach((e) => {
            var newNum = document.createElement("p");
            newNum.innerHTML = e;
  
            dateContainer.append(newNum);
          });
        }
      });
  

      

    });
});
})(jQuery, this);

  