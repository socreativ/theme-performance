<?php if(my_wp_is_mobile() && !get_field('isMobile')): else: ?>

<section id="<?=  $block['anchor'] ?>" class="partenaires <?=  $block['className'] ?>">

    <?php if(get_field('isHR')): ?><hr id="partenaires-hr"><?php endif; ?>
    <h2 class="partenaires-title"><?php the_field('title'); ?></h2>

    <?php if(have_rows('partenaires')): ?>
    <div class="container partenaires-container">

        <div class="partenaires-prev-arrow">
            <svg width="100%" height="100%" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>Arrow</title>
                <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Group-19" transform="translate(0.000000, -0.000000)" fill="#FFFFFF" fill-rule="nonzero">
                        <g id="arrow-down" transform="translate(6.000000, 3.000000)">
                            <polygon id="Stroke-1" transform="translate(5.500000, 8.389086) rotate(-90.000000) translate(-5.500000, -8.389086) " points="5.5 10.5378859 12.2704628 3.3160589 13.7295372 4.6839411 5.5 13.4621141 -2.7295372 4.6839411 -1.2704628 3.3160589"></polygon>
                        </g>
                    </g>
                </g>
            </svg>
        </div>

        <div class="partenaires-slider text-center">
            <?php while(have_rows('partenaires')): the_row(); ?>
                <?php if(get_sub_field('logo')['isSVG']): ?>
                    <?=  get_sub_field('logo')['svg'] ?>
                <?php else: ?>
                    <img class="skip-lazy partenaires-logo" data-skip-lazy="" src="<?=  get_sub_field('logo')['image']['url'] ?>" alt="<?=  get_sub_field('logo')['image']['alt'] ?>" width="250" height="125"/>
                <?php endif; ?>
            <?php endwhile; ?>
        </div>

        <div class="partenaires-next-arrow">
            <svg width="100%" height="100%" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>Arrow</title>
                <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Group-19" transform="translate(0.000000, -0.000000)" fill="#FFFFFF" fill-rule="nonzero">
                        <g id="arrow-down" transform="translate(6.000000, 3.000000)">
                            <polygon id="Stroke-1" transform="translate(5.500000, 8.389086) rotate(-90.000000) translate(-5.500000, -8.389086) " points="5.5 10.5378859 12.2704628 3.3160589 13.7295372 4.6839411 5.5 13.4621141 -2.7295372 4.6839411 -1.2704628 3.3160589"></polygon>
                        </g>
                    </g>
                </g>
            </svg>
        </div>

    </div>
    <?php endif; ?>

</section>

<?php endif; ?>