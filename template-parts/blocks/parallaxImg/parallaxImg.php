<?php

$txt = get_field('texte');

$img1 = get_field('img1');
$img2 = get_field('img2');
$img3 = get_field('img3');




?>

<section id="<?=  $block['anchor'] ?>" class="parallaxImg <?=  $block['className'] ?>">
    <div class="container">
        <div class="row">

            <div class="col-md-6 text-center">
                <?php if ($img1['scale']) : ?>
                    <div class="elem-para position-relative position-md-absolute z-9 overflow-hidden text-container">
                <?php endif; ?>
                    <?php
                    $css = $img1['scale'] ? "h-100 w-100 elem-scale" : "elem-para text-container w-100";
                    $img1Url = $img1['image']['url'];
                    $ext = pathinfo($img1Url, PATHINFO_EXTENSION);
                    if( $ext == 'gif' ):?>
                        <img class="<?=  $css; ?> custom-lazy" src="" custom-lazy-src="<?=  $img1Url; ?>" alt="<?=  $img1['image']['alt'] ?>">
                    <?php else:?>
                        <img class="<?=  $css; ?>" src="<?=  $img1Url; ?>" alt="<?=  $img1['image']['alt'] ?>">
                    <?php endif; ?>
                <?php if ($img1['scale']) : ?>
                  </div>
                <?php endif; ?>
            </div>


            <div class="col-md-6 text-center text-md-left">
                <p class="fs-18 mt-80 mb-80 text-container mx-auto mx-md-0"><?=  $txt ?></p>
                <div class="position-relative">
                <?php if ($img2['scale']) : ?>
                    <div class="elem-para position-relative position-md-absolute z-9 overflow-hidden text-container">
                <?php endif; ?>
                    <?php
                    $css = $img2['scale'] ? "h-100 w-100 elem-scale" : "elem-para text-container w-100";
                    $img2Url = $img2['image']['url'];
                    $ext = pathinfo($img2Url, PATHINFO_EXTENSION);
                    if( $ext == 'gif' ):?>
                        <img class="<?=  $css; ?> custom-lazy" src="" custom-lazy-src="<?=  $img2Url; ?>" alt="<?=  $img2['image']['alt'] ?>">
                    <?php else:?>
                        <img class="<?=  $css; ?>" src="<?=  $img2Url; ?>" alt="<?=  $img2['image']['alt'] ?>">
                    <?php endif; ?>
                <?php if ($img2['scale']) : ?>
                  </div>
                <?php endif; ?>
                </div>
            </div>


            <div class="col-12 img-para d-none d-md-block text-center text-md-left">
                <?php if ($img3['scale']) : ?>
                    <div class="elem-para position-relative position-md-absolute z-9 overflow-hidden text-container">
                <?php endif; ?>
                    <?php
                    $css = $img3['scale'] ? "h-100 w-100 elem-scale" : "elem-para text-container w-100";
                    $img3Url = $img3['image']['url'];
                    $ext = pathinfo($img3Url, PATHINFO_EXTENSION);
                    if( $ext == 'gif' ):?>
                        <img class="<?=  $css; ?> custom-lazy" src="" custom-lazy-src="<?=  $img3Url; ?>" alt="<?=  $img3['image']['alt'] ?>">
                    <?php else:?>
                        <img class="<?=  $css; ?>" src="<?=  $img3Url; ?>" alt="<?=  $img3['image']['alt'] ?>">
                    <?php endif; ?>
                <?php if ($img3['scale']) : ?>
                  </div>
                <?php endif; ?>
            </div>


        </div>
    </div>
</section>
