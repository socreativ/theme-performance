("use strict");
(function ($, root, undefined) {
    $(function () {
    $(document).ready(function () {

        /* Parallax */
        var bodyW = $(window).width();
        var parallaxController = new ScrollMagic.Controller({});

        $(".parallaxImg").each(function () {
            if (bodyW > 768) {
            $(".parallaxImg").each(function () {
                var imgs = this.querySelectorAll(".elem-para");

                // img 1
                new ScrollMagic.Scene({
                triggerElement: ".parallaxImg",
                triggerHook: 0.65,
                duration: "120%",
                })
                .setTween(imgs[0], { y: 50, ease: Linear.easeNone })
                .addTo(parallaxController);

                // img2
                new ScrollMagic.Scene({
                triggerElement: ".parallaxImg",
                triggerHook: 0.65,
                duration: "120%",
                })
                .setTween(imgs[1], { y: 150, ease: Linear.easeNone })
                .addTo(parallaxController);
                new ScrollMagic.Scene({
                triggerElement: ".parallaxImg",
                triggerHook: 0.65,
                duration: "120%",
                })
                .setTween(".img-para", { y: 250, ease: Linear.easeNone })
                .addTo(parallaxController);

                // scale img
                new ScrollMagic.Scene({
                triggerElement: ".parallaxImg",
                triggerHook: 0.65,
                duration: "120%",
                })
                .setTween(".elem-scale", { scale: 1.2, ease: Linear.easeNone })
                // .addIndicators()
                .addTo(parallaxController);
            });
            }
        });


    });
});
})(jQuery, this);
