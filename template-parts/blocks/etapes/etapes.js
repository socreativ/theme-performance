("use strict");
(function ($, root, undefined) {
    $(function () {
    $(document).ready(function () {


    /**
     *  Slider steps (Block - Étapes)
     */
    $('.etapes').each(function  (){

        let slickImg, slickTxt;

        $('.steps-img-slider').each(function (){
            slickImg = $(this).slick({
            lazyLoad: 'progressive',
            autoplay: false,
            dots: false,
            arrows: false,
            infinite: false,
            draggable: false,
            swipe: false,
            fade: true,
            adaptiveHeight: true,
            });
        });

        $('.content-slider').each(function (){
            slickTxt = $(this).slick({
            lazyLoad: 'progressive',
            autoplay: false,
            dots: false,
            arrows: false,
            infinite: false,
            draggable: false,
            fade: true,
            adaptiveHeight: false,
            responsive: [
                {
                breakpoint: 768,
                settings: {
                    adaptiveHeight: true
                }
                }
            ]
            });
        });

        $('.content-slider').each(function (){
            $(this).on('swipe', function(e, s, d){
            let i = $('.trigger.active').attr('trigger-id');
            let l = $('.trigger').length;
            console.log(l)

            if(d == 'right' && i > 0) i--;
            if(d == 'left' && i < l-1) i++;
            swipeSteps(i)
            })
        })

        $('.trigger').each(function (){
            $(this).click(function (){
                let i = $(this).attr('trigger-id');
                swipeSteps(i)
            })
        });


        /**
         *
         * @param {int} i - index
         * @param {int} p - position
         * @param {int} l - trigger's length
         */
        function swipeSteps(i){
            let l = $('.trigger').length;
            slickTxt.slick('slickGoTo', i);
            slickImg.slick('slickGoTo', i);
            $('.trigger').each(function(){$(this).removeClass('active')});
            $('.trigger-spacer').each(function(){$(this).removeClass('big')});
            if(i < l-1) $($('.trigger-spacer')[i]).addClass('big');
            $($('.trigger')[i]).addClass('active');
            let p = parseInt(i) * 100;
            $('.steps-content-slider').css({"left": p+40+'px'});
        }


        });
            

    });
});
})(jQuery, this);

    


