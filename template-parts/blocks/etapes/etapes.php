<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$title_el = get_field('title_el');
$title = get_field('title');
$titleIsUpCase = get_field('title_capitalize');
$subtitle = get_field('subtitle');
$subtitle_el = get_field('subtitle_el');
$subtitleIsUpCase = get_field('subtitle_capitalize');
$maskOn = get_field('masque_sur_les_images_condition');
if ($maskOn) {
  $classmaskOn = '';
}else {
  $classmaskOn = 'masked-element';
}

?>

<section id="<?= $id ?>" class="etapes <?= $css ?>">

    <div class="container text-center pt-5 pb-3 title-etapes <?=  $classmaskOn ?>">
        <<?=  $title_el ?> class="mb-2 section-title <?php if($titleIsUpCase) echo 'text-uppercase'; ?> fw-800 has-white-color"><?=  $title; ?></<?=  $title_el ?>>
        <<?=  $subtitle_el ?> class="mb-2 section-subtitle <?php if($subtitleIsUpCase) echo 'text-uppercase'; ?> has-blue-color fw-800 "><?=  $subtitle; ?></<?=  $subtitle_el ?>>
    </div>

    <div class="masked-image <?=  $classmaskOn ?>">
        <?php if ($maskOn) { ?>
          <?php if (!my_wp_is_mobile()) { ?>
            <div class="mask">
              <div class="mask-container h-100 col-12 col-lg-9">
                <?php
                $image = get_field('masque_sur_les_images');
                if ( !empty($image) ): ?>
                  <img class="skip-lazy" src="<?=  $image['url']; ?>" alt="<?=  $image['alt']; ?>" />
                <?php endif; ?>
              </div>
            </div>
          <?php } ?>
        <?php } ?>
        <div class="steps-img-slider">
        <?php
            if(have_rows('elements')):
                while(have_rows('elements')): the_row();
                    $image = get_sub_field('image');
                    if($image): ?>
                        <?php if(my_wp_is_mobile()): ?>
                            <img class="etapes-image" data-skip-lazy="" src="" data-lazy="<?=  $image['sizes']['large']; ?>"  alt="<?=  $image['alt']; ?>">
                        <?php else: ?>
                            <img class="etapes-image" data-skip-lazy="" src="" data-lazy="<?=  $image['url']; ?>"  alt="<?=  $image['alt']; ?>">
                        <?php endif; ?>
                    <?php endif;
                endwhile;
            endif;

        ?>
        </div>
    </div>

    <div class="container text-center steps-content position-relative <?=  $classmaskOn ?>">

        <div class="triggers d-flex">
            <?php if(have_rows('elements')): $i = 0;
                while(have_rows('elements')): the_row(); $i++;
                    $nbr = get_sub_field('etapes');
                    if($nbr): ?>

                        <?php if($i!=1): ?>
                            <div class="trigger-spacer <?php if($i==2) echo 'big'; ?>"></div>
                        <?php endif; ?>

                        <div class="trigger anim-300 <?php if($i==1) echo 'active'; ?>" trigger-id="<?=  $i-1 ?>"><p class="number-steps anim-300"><?=  $nbr; ?></p><span class="trigger-bar anim-300"></span></div>

                    <?php endif;
                endwhile;
            endif; ?>
        </div>

        <div class="steps-content-slider anim-300">
            <div class="content-slider">
                <?php

                    if(have_rows('elements')):
                        while(have_rows('elements')): the_row();
                            $title = get_sub_field('title');
                            $subtitle = get_sub_field('subtitle');
                            $content = get_sub_field('content');
                        ?>
                        <?php if($content): ?>

                            <div class="content-slide">
                                <h5 class="steps-title"><?=  $title ?></h5>
                                <h6 class="steps-subtitle"><?=  $subtitle ?></h6>
                                <p class="steps-content-txt text-justify"><?=  $content ?></p>
                            </div>

                        <?php endif;
                        endwhile;
                    endif;

                ?>
            </div>
        </div>

    </div>

</section>
