<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$margin = my_wp_is_mobile() ? get_field('mobile') : get_field('desktop');

?>

<div id="<?= $id ?>" class="spacer-block <?= $css ?>" style="height: <?= $margin; ?>vh;"></div>