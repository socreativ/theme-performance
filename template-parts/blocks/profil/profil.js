("use strict");
(function ($, root, undefined) {
    $(function () {
    $(document).ready(function () {


        var profilController = new ScrollMagic.Controller({});

        $('.profil-parallax').each(function (){
            new ScrollMagic.Scene({
                triggerElement: ".profil-parallax",
                triggerHook: 0.65,
                duration: "120%",
            })
            .setTween(this, { y: -350, ease: Power1.easeInOut })
            .addTo(profilController);
        })
        

    });
});
})(jQuery, this);