

<section class="profil <?=  $block['className'] ?>" id="<?= $block['anchor'] ?>">

    <div class="container">
        <div class="row <?php if(get_field('reverse')) echo "flex-md-row-reverse" ?>">

            <div class="col-md-6">
                <?php $img = get_field('image'); ?>
                <img class="<?php $css = my_wp_is_mobile() ? 'profil-picture' : 'profil-parallax'; echo $css; ?>" src="<?=  $img['url']; ?>" alt="<?=  $img['alt']; ?>" height="<?=  $img['height']; ?>" width="<?=  $img['width']; ?>"/>
            </div>
            <div class="col-md-6">
                <div class="content has-white-color">
                    <?=  get_field('texte') ?>
                </div>
            </div>

        </div>
    </div>

</section>