("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {

         /**
         *  MAIN SLIDER (Block - Slider)
         *   */
        $('.main-slider').each(function (){
            const speed = $(this).attr('speed');
            const animSpeed = $(this).attr('anim-speed');
            const asNavFor = []

            $('.title-slider').each(function (){
              asNavFor.push('.title-slider')
              $(this).slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                vertical: true,
                zIndex: 2,
                asNavFor: '.main-slider'
              });
            });
    
    
            $('.subtitle-slider').each(function (){
              asNavFor.push('.subtitle-slider')
              $(this).slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                vertical: true,
                zIndex: 2,
                asNavFor: '.main-slider'
              });
            });

            $(this).slick({
              lazyLoad: 'progressive',
              autoplay: true,
              autoplaySpeed: speed,
              speed: animSpeed,
              dots: true,
              asNavFor: asNavFor.toString(),
              appendDots:$('#slider-dots'),
              dotsClass:'slider-dots',
              arrows: true,
              prevArrow: $('.slider-prev-arrow'),
              nextArrow: $('.slider-next-arrow'),
              draggable: false,
              fade: true,
              focusOnSelect: true,
              zIndex: 2,
              responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false
                  }
                }
              ]
            });
        });

  
      

    });
});
})(jQuery, this);

  