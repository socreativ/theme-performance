<?php
# Get ACF
# slider options
$type = get_field('my_slider_type');
$speed = get_field('param')['speed'];
$anim_speed = get_field('param')['anim_speed'];

# video
if(get_field('url')){
  $videomp4 = get_field('url')['slider_video_mp4'] ? get_field('url')['slider_video_mp4'] : null;
  $videowebm = get_field('url')['slider_video_webm'] ? get_field('url')['slider_video_webm'] : null;
  $videoposter = get_field('url')['slider_video_poster'] ? get_field('url')['slider_video_poster'] : null;
}

# images
$isDiapo = get_field('isSingle');
$images = get_field('images_diapo') ? get_field('images_diapo') : null;
$image_single = get_field('image_single') ? get_field('image_single') : null;

# title
$is_single_title = !get_field('single_title');
if($is_single_title){
  $title = get_field('title')['text'];
  $title_css = get_field('title')['css'];
  $title_el = get_field('title')['balise'];
  $title_attr = get_field('title')['attr'];
}else{
  $titles = get_field('multiple_titles')['titles'];
  $title_el = get_field('multiple_titles')['balise'];
  $title_css = get_field('multiple_titles')['css'];
}

# subtitle
$is_single_subtitle = !get_field('single_subtitle');
if($is_single_subtitle){
  $subtitle = get_field('subtitle')['text'];
  $subtitle_css = get_field('subtitle')['css'];
  $subtitle_el = get_field('subtitle')['balise'];
  $subtitle_attr = get_field('subtitle')['attr'];
}else{
  $subtitles = get_field('multiple_subtitles')['subtitles'];
  $subtitle_el = get_field('multiple_subtitles')['balise'];
  $subtitle_css = get_field('multiple_subtitles')['css'];
}

$alpha = get_field('alpha');

# short text
$short = get_field('short') ? get_field('short') : null;

?>

<!-- SLIDER BEGIN -->
<section id="<?=  $block['anchor'] ?>" class="slider <?=  $block['className'] ?>">
    <div class="greyscale" style="opacity: 0.<?= $alpha ?>"></div>
    <?php   if(!$type): ?>


      <?php if(!$isDiapo): ?>
          <img src="<?=  $image_single['sizes']['medium'] ?>" alt="<?=  $image_single['alt'] ?>"/>
      <?php else:
        if($images && !my_wp_is_mobile()): ?>

        <div class="slider-container">

          <div class="slider-prev-arrow">
              <svg width="100%" height="100%" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <title>Arrow</title>
                  <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Group-19" transform="translate(0.000000, -0.000000)" fill="#FFFFFF" fill-rule="nonzero">
                          <g id="arrow-down" transform="translate(6.000000, 3.000000)">
                              <polygon id="Stroke-1" transform="translate(5.500000, 8.389086) rotate(-90.000000) translate(-5.500000, -8.389086) " points="5.5 10.5378859 12.2704628 3.3160589 13.7295372 4.6839411 5.5 13.4621141 -2.7295372 4.6839411 -1.2704628 3.3160589"></polygon>
                          </g>
                      </g>
                  </g>
              </svg>
          </div>

          <div class="main-slider" speed="<?=  $speed ?>" anim-speed="<?=  $anim_speed ?>">

        <?php foreach ($images as $single): ?>
            <img class="main-slider-bkg" data-skip-lazy="" src="" data-lazy="<?=  esc_url($single['url']) ?>" alt="<?=  $single['alt'] ?>">
        <?php  endforeach; ?>


          </div>

          <div class="slider-next-arrow">
              <svg width="100%" height="100%" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <title>Arrow</title>
                  <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Group-19" transform="translate(0.000000, -0.000000)" fill="#FFFFFF" fill-rule="nonzero">
                          <g id="arrow-down" transform="translate(6.000000, 3.000000)">
                              <polygon id="Stroke-1" transform="translate(5.500000, 8.389086) rotate(-90.000000) translate(-5.500000, -8.389086) " points="5.5 10.5378859 12.2704628 3.3160589 13.7295372 4.6839411 5.5 13.4621141 -2.7295372 4.6839411 -1.2704628 3.3160589"></polygon>
                          </g>
                      </g>
                  </g>
              </svg>
          </div>

          <div id="slider-dots">

          </div>

        </div>
      <?php else:?>
        <img src="<?=  $images[0]['sizes']['large']; ?>" alt="<?= $images[0]['alt'] ?>"/>

      <?php endif;
      endif; ?>
    <?php else: ?>
      <?php if(!my_wp_is_mobile()): ?>
      <video playsinline="" autoplay="" loop="" muted="" src="<?=  $videomp4; ?>" poster="<?=  $videoposter; ?>">
        <source src="<?=  $videowebm; ?>"
                type="video/webm">
        <source src="<?=  $videomp4; ?>"
                type="video/mp4">
      </video>
      <?php else: ?>
        <img src="<?= $videoposter ?>" alt="video poster">
      <?php endif; ?>
    <?php endif; ?>
    
      <div class="slider-content">
        <div class="slider-text">
        <?php if($is_single_title): #single title?>
              <?php if($title_el): ?>
                <<?=  $title_el; ?> class="<?=  $title_css; ?>" <?=  $title_attr; ?>><?=  $title; ?></<?=  $title_el; ?>>
              <?php endif; ?>
        <?php else: #title > 1 ?>
          <?php if(!my_wp_is_mobile()): ?>
            <?php if(have_rows('multiple_titles')): while(have_rows('multiple_titles')): the_row(); ?>
              <?php if(have_rows('titles')): ?>
                <<?=  $title_el; ?> class="<?=  $title_css; ?> title-slider">
                  <?php while(have_rows('titles')): the_row(); #foreach titles ?>
                    <span class="<?= get_sub_field('title')['css'] ?>"><?=  get_sub_field('title')['text']; ?></span>
                  <?php endwhile ?>
                </<?=  $title_el; ?>>
              <?php endif; ?>
            <?php endwhile; endif; ?>
            <?php else: ?>
              <<?= $title_el ?> class="<?=  $title_css; ?>" <?=  $title_attr; ?>><?=  get_field('multiple_titles')['titles'][0]['title']['text']; ?></<?=  $title_el; ?>>
            <?php endif; ?>
        <?php endif; ?>

        <?php if($is_single_subtitle): #single subtitle?>
              <?php if($subtitle_el): ?>
                <<?=  $subtitle_el; ?> class="<?=  $subtitle_css; ?>" <?=  $subtitle_attr; ?>><?=  $subtitle; ?></<?=  $subtitle_el; ?>>
              <?php endif; ?>
        <?php else: #subtitle > 1 ?>
        <?php if(!my_wp_is_mobile()): ?>
          <?php if(have_rows('multiple_subtitles')): while(have_rows('multiple_subtitles')): the_row(); ?>
                <?php if(have_rows('subtitles')): $i=0; ?>
                  <<?= $subtitle_el; ?> class="<?= $subtitle_css; ?> subtitle-slider">
                  <?php while(have_rows('subtitles')): the_row(); $i++; #foreach titles ?>
                      <span class="<?= get_sub_field('subtitle')['css'] ?>"><?=  get_sub_field('subtitle')['text']; ?></span>
                    <?php endwhile ?>
                  </<?=  $subtitle_el; ?>>
                <?php endif; ?>
              <?php endwhile; endif; ?>
            <?php else: ?>
              <<?= $subtitle_el ?> class="<?=  $subtitle_css; ?>" <?=  $subtitle_attr; ?>><?=  get_field('multiple_subtitles')['subtitles'][0]['subtitle']['text']; ?></<?=  $subtitle_el; ?>>
            <?php endif; ?>
        <?php endif; ?>
        <?php if($short): ?>
          <div class="short">
            <?= $short ?>
          </div>
        <?php endif; ?>

        <?php if(have_rows('btn')):
                while(have_rows('btn')): the_row();
                    $link = get_sub_field('link');
                    $css = get_sub_field('css_class'); ?>
                    <a href="<?=  $link['url']; ?>" class="wp-block-button__link <?=  $css; ?>" target="<?=  $link['target']; ?>"><?=  $link['title']; ?></a>
                <?php endwhile;
            endif; ?>
        </div>
      </div>
    <div class="trigger-scroll-debug"></div>
    <?php wp_reset_query(); ?>


    
    <div class="arrows">
			<svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
			<path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
			</svg>
			<svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
				<path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
			</svg>
			<svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
				<path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
			</svg>
			<svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
				<path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
			</svg>
		</div>
</section>
<!-- END SLIDER -->
