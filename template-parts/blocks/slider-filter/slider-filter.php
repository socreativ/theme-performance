<?php 



$title_el = get_field('el_title');
$title = get_field('title');
$titleIsUpCase = get_field('title_capitalize');
$subtitle = get_field('subtitle');
$subtitle_el = get_field('el_subtitle');
$subtitleIsUpCase = get_field('subtitle_capitalize');
$text_bkg = get_field('text_bkg');


?>

<?php if(my_wp_is_mobile() && !get_field('isMobile')): else: ?>

<section id="<?=  $block['anchor'] ?>" class="slider-filter <?=  $block['className'] ?>">

    <div class="container text-center">
        <<?=  $title_el ?> class="mb-2 section-title <?php if($titleIsUpCase) echo 'text-uppercase'; ?> fw-800 "><?=  $title; ?></<?=  $title_el ?>>
        <<?=  $subtitle_el ?> class="mb-2 section-subtitle <?php if($subtitleIsUpCase) echo 'text-uppercase'; ?> fw-800 "><?=  $subtitle; ?></<?=  $subtitle_el ?>>
    </div>

    <div class="text-center mt-4">
    <?php if ( have_rows('filter') ): ?>
        <div class="filtre">
        
        <a href="javascript:void(0)" class="filter select" filter="<?=  'all' ?>">Tous</a>
        <?php while ( have_rows('filter') ) : the_row(); ?>
            <?php
            $label = get_sub_field('label');
            $target = get_sub_field('target');
            if( $label && $target ): ?>
                    <a href="javascript:void(0)" class="filter" filter="<?=  $target ?>"><?=  $label ?></a>
            <?php endif; ?>
        <?php endwhile; ?>
        </div>
    <?php endif; ?>

    <?php if ( have_rows('filter') ): ?>

        <div class="modele-slider mt-4">
            <?php while ( have_rows('filter') ) : the_row(); ?>
                <?php $target = get_sub_field('target'); ?>
                <?php if ( have_rows('modele') ): ?>
                    <?php while ( have_rows('modele') ) : the_row(); ?>
                        <div class="modele-slide" data-category="<?=  $target; ?>">

                        <?php 
                        $category = get_sub_field('category');
                        if($category): 
                        $first_row = true;
                        
                            while( have_rows('gallery') ) : the_row(); ?>
                                <?php if($first_row): ?>
                                    <a href="<?=  get_sub_field('image')['url'] ?>" data-fancybox="<?=  $category['titre'] ?>" data-caption="<?=  $category['caption']; ?>">
                                        <img src="<?=  $category['image']['sizes']['square-thumb'] ?>" alt="<?=  $category['image']['alt'] ?>">
                                        <div class="cat-greyscale"></div>
                                        <h5><?=  $category['titre'] ?></h5>
                                    </a>
                                    
                                    <div class="modele-sizes" id="<?=  $category['titre'].'-sizes' ?>">
                                        <hr>
                                        <p class="modele-sizes-captions">
                                            <?=  $category['caption']; ?>
                                        </p>
                                    </div>

                                <?php $first_row = false; else: ?>
                                    <a hidden href="<?=  get_sub_field('image')['url']; ?>" data-fancybox="<?=  $category['titre']; ?>" data-caption="<?=  $category['caption']; ?>">
                                        <img src="<?=  get_sub_field('image')['sizes']['thumbnail']; ?>">
                                    </a>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>
            <?php endwhile; ?>
        </div>

        <div class="modele-slider-arrows">
            <button class="modele-prev-arrows" aria-label="slider-filter-prev">
                <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
                </svg>
            </button>
            <button class="modele-next-arrows" aria-label="slider-filter-next">
                <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
                </svg>
            </button>
        </div>

    <?php endif; ?>


       
    </div>

    <?php if($text_bkg): ?>
    <svg viewBox="0 0 8 2" class="slider-txt-bkg w-100">
        <text x="4" y="1.9" text-anchor="middle" font-size="2" fill="none" stroke="var(--grey)" stroke-width=".015" font-family="sans-serif" font-weight="bold" style="text-transform: uppercase;">
            <?=  $text_bkg; ?>
        </text>
    </svg>
    <?php endif; ?>

</section>

<?php endif; ?>