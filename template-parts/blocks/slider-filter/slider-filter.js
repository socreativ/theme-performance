("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {


 /**
         *  SLIDER w/ Filter (Block - Slider w/ Filter)
         *   */

        $('.modele-slider').each(function (){
            let modelSlider = $(this).slick({
              lazyLoad: 'progressive',
              autoplay: false,
              dots: false,
              infinite: false,
              draggable: true,
              arrows: true,
              slidesToShow: 4,
              slideToScroll: 1,
              adaptiveHeight: false,
              prevArrow: $('.modele-prev-arrows'),
              nextArrow: $('.modele-next-arrows'),
              responsive: [
                {
                  breakpoint: 1450,
                  settings: {
                    slidesToShow: 3,
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                  }
                }
              ]
            });
  
            $('.filter').each(function (){
              $(this).on('click', function(){
                $('.filter').each(function (){
                  $(this).removeClass('select');
                })
                $(this).addClass('select');
                let filter = $(this).attr('filter');
                if(filter == 'all'){
                  modelSlider.slick('slickUnfilter')
                }else {
                  modelSlider.slick('slickUnfilter')
                  modelSlider.slick('slickFilter', '.modele-slide[data-category='+filter+']')
                }
                //console.log($('.modele-slider-arrows .slick-hidden'))
                if($('.modele-slider-arrows .slick-hidden').length > 0){
                  $('.modele-slider-arrows').addClass('d-none');
                }
                else{
                  $('.modele-slider-arrows').removeClass('d-none');
                }
  
              });
            });
  
          });
  
      

    });
});
})(jQuery, this);

  