<?php

$dir = get_field('reverse') ? 'flex-row-reverse' : 'flex-row';

?>

<?php if(my_wp_is_mobile() && !get_field('isMobile')): else: ?>


<!-- BEGIN TOGGLE SLIDER -->
<section class="toggle-slider-section <?=  $block['className'] ?>" id="<?=  $block['anchor'] ?>">

    <div class="container">

        <?php if ( have_rows('content') ): $i = -1; ?>
          <div class="toggle-categorie">
            <?php while ( have_rows('content') ) : the_row(); $i++ ?>
              <?php
                $categorie = get_sub_field('categorie');
                if( $categorie ): ?>
                       <?php if($i != 0) echo '<span> | </span>'; ?><p class="toggle-toggle <?php if($i == 0) echo 'select' ?>" slideToGo="<?=  $i ?>"><?=  $categorie ?></p>
                <?php endif; ?>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>

        <?php if ( have_rows('content') ): ?>
          <div class="toggle-slider">
            <div class="row position-relative <?= $dir; ?>">

            <div class="col-md-12 col-lg-6 toggle-text-slider">
                <div class="toggle-content-spacer"></div>
                <div class="toggle-text-slick">
                <?php while ( have_rows('content') ) : the_row(); ?>
                <?php
                    $text = get_sub_field('text_content'); ?>

                    <div class="h-100 d-flex justify-content-center align-items-center text-justify">
                        <div class="">
                            <?=  $text; ?>
                        </div>
                    </div>

                <?php endwhile; ?>
                </div>
                <div class="toggle-content-spacer"></div>
            </div>
            <div class="toggle-image-slider">
              <?php while ( have_rows('content') ) : the_row(); ?>
                <?php
                $image = get_sub_field('image_content');
                ?>
                <img class="toggle-image-slide" data-skip-lazy="" src="" data-lazy="<?=  $image['url'] ?>" alt="<?=  $image['alt']?>"/>
              <?php endwhile; ?>
            </div>
            </div>
          </div>
        <?php endif; ?>

    </div>


</section>
<!-- END TOGGLE SLIDER -->

<?php endif; ?>