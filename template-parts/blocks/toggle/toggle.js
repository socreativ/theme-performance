("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {


        /**
         *  TOGGLE SLIDER (Block - toggle-slider)
         *   */

        $('.toggle-slider-section').each(function (){

            let slick, slick2;
  
            slick = $('.toggle-text-slick').not('.slick-initialized').slick({
              lazyLoad: 'progressive',
              autoplay: false,
              dots: false,
              infinite: false,
              draggable: false,
              fade: true,
              arrows: false,
            });
  
            slick2 = $('.toggle-image-slider').not('.slick-initialized').slick({
              lazyLoad: 'progressive',
              autoplay: false,
              dots: false,
              infinite: false,
              draggable: false,
              swipe: false,
              arrows: false,
              adaptiveHeight: true,
            });
       
  
            $('.toggle-text-slider').each(function (){
              $(this).on('swipe', function(e, s, d){
                let i = $(".toggle-toggle.select").attr('slidetogo');
                let l = $('.toggle-toggle').length;
  
                if(d == 'right' && i > 0) i--;
                if(d == 'left' && i < l-1) i++;
                swipeToggle(i)
              })
            })
  
            $('.toggle-toggle').each(function (){
              $(this).click(function () {
                  i = $(this).attr('slidetogo');
                  swipeToggle(i)
              });
            });
  
            function swipeToggle(i){
              $('.toggle-toggle').each(function (){
                $('.toggle-toggle').removeClass('select');
              })
  
              $($('.toggle-toggle')[i]).addClass('select');
              slick.slick('slickGoTo', i);
              if($(window).width() > 768) slick2.slick('slickGoTo', i);
            }

            function hideInMobile(){
              if(window.innerWidth < 1024){
                $('.toggle-image-slider').hide();
              }
              else{
                $('.toggle-image-slider').show();
                slick2.slick('refresh');
              }
            }

            hideInMobile();
            window.addEventListener('resize', hideInMobile);
  
          });
      

    });
});
})(jQuery, this);

  