<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */


    global $woocommerce;
    $product = wc_get_product( get_the_ID() );

    $taxo = get_the_terms(get_the_ID(), 'product_cat');
    $taxo = array_filter($taxo, function($t){return $t->parent !== 0;});
    usort($taxo, function($a, $b){return $b->parent - $a->parent;});

    $taxoFilter = [];
    foreach ($taxo as $el) {
            $taxoFilter[get_term($el->parent)->name][$el->name] = get_term_link($el->term_id);
    }

    $related_taxo = $taxo[rand(0, count($taxo)-1)];
    $rp_query = new WP_Query(array(
        'post_type' => get_post_type(),
        'posts_per_page' => 4,
        'tax_query' => array( array(
            'taxonomy' => $related_taxo->taxonomy,
            'field' => 'slug',
            'terms' => $related_taxo->slug,
        )),
    ));
    $related_posts = $rp_query->posts;
    foreach ($related_posts as $id => $r_post) {
        if($r_post->ID == get_the_ID()) unset($related_posts[$id]);
    }

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6">
                <div class="product-gallery-container">
                    <div class="product-gallery-sticky">
                        <a data-fancybox="gallery" href="<?= wp_get_attachment_image_url($product->get_image_id(), 'full') ?>">
                            <img src="<?= wp_get_attachment_image_url($product->get_image_id(), 'full') ?>" alt="product thumb">
                        </a>
                        <?php if($product->get_gallery_image_ids()): ?>
                            <?php foreach ($product->get_gallery_image_ids() as $image_id): ?>
                                <a data-fancybox="gallery" href="<?= wp_get_attachment_image_url($image_id, 'full') ?>">
                                    <img src="<?= wp_get_attachment_image_url($image_id, 'full') ?>" alt="product thumb">
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>


                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 product-information">
                <a class="back-to-archive mb-5 d-block" href="<?= get_post_type_archive_link(get_post_type()); ?>"><img src="<?= get_stylesheet_directory_uri() . "/assets/img/left-arrow.svg"; ?>">Retour</a>
                <h1 class="product-name"><?= the_title(); ?></h1>
                <p class="post-taxo">
                <?php $i=0; foreach($taxoFilter as $parent => $children): ?>
                    <?= $parent . " : " ?>

                    <?php $j =0; foreach($children as $name => $link): ?>
                        <a href="<?= $link; ?>"><?= $name; ?><?php if(count($children) > 1 && $j != count($children) -1) echo ', '; ?></a>
                    <?php $j++; endforeach; ?>

                    <?php if(count($taxoFilter) > 1 && $i != count($taxoFilter) - 1): if(!my_wp_is_mobile()): echo ' | '; else: echo '<br>'; endif; endif;?>
                <?php $i++; endforeach; ?>
                </p>

                <div class="product-price">
                    <p><?= $product->get_price() . get_woocommerce_currency_symbol() ?></p>
                </div>
                <a class="buy button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?= $product->get_id() ?>" href="?add-to-cart=<?= $product->get_id() ?>" target="_blank"><img src="<?= get_stylesheet_directory_uri() . "/assets/img/bag-09.svg"; ?>" alt="cart svg"> Ajouter au panier</a>

                <div class="product-desc">
                    <ul class="tabs">
                        <li class="desc-tab-tgl ml-0 active">Description</li>
                        
                    </ul>
                <div class="desc-tab">
                    <?php the_content(); ?>
                </div>

            </div>

            <?php if($related_posts): ?>
            <div class="related-posts">
                <h2 class="similar-posts">Posts similaires lié à <a href="<?= get_term_link($related_taxo->term_id); ?>"><?= $related_taxo->name ?></a> : </h2>   
                <div class="row">
                    <?php foreach($related_posts as $c_post): ?>         
                        <div class="related-products mb-3">
                            <a class="product-inner-link" href="<?= get_permalink(); ?>">
                                <div class="product-inner-wrapper">
                                    <img class="attachment-post-thumbnail wp-post-image" src="<?= get_the_post_thumbnail_url($c_post->ID) ?>" alt="post thumbnail" />
                                    <div class="product-title">
                                        <div class="d-flex">
                                            <p class="w-100"><?= $c_post->post_title ?></p>
                                           
                                            <img src="<?= get_stylesheet_directory_uri() . "/assets/img/arrow.svg"; ?>" alt="">
                                        </div>
                                        
                                    </div>
                                 
                                </div>
                            </a>
                            <div class="d-flex justify-content-between mt-3 align-items-center">
                                <p class="price my-0 mx-3"><?= wc_get_product($c_post)->get_price() . get_woocommerce_currency_symbol() ?></p>
                                <a class="button add_to_cart_button ajax_add_to_cart" href="?add-to-cart=<?= $c_post->ID ?>">Ajouter au panier</a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <a class="see-all" href="<?= get_term_link($related_taxo->term_id); ?>">Voir tout</a>

            </div>
            <?php endif; ?>

           

            <div style="height:5vh;"></div>
            <p>Vous êtes intéressé ? <a class="bold" href="<?= get_home_url(); ?>?ref=<?= get_field('ref', get_the_ID()); ?>#contact">Contactez-nous</a></p>
            <div style="height:5vh;"></div>

        </div>
    </div>
	
</article><!-- #post-<?php the_ID(); ?> -->
