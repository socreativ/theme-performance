<!-- BUILDING MODE -->
	<div class="text-building-mode no-bkg">
		<div class="w-100 h-100 position-relative">


			<div id="" class="jumbotron jumbotron-big <?php if (wp_is_mobile()) {echo "jumbotron-mobile";} ?> jumbotron-fluid p-0 mb-0 no-bkg position-relative">


				<div class="h-100 w-100 position-absolute t-0">
					<div class="jumb-container position-sticky d-flex flex-column justify-content-center align-items-center">

						<?php if (wp_is_mobile()) : ?>
								<!-- VERSION MOBILE -->
							<?php else : ?>
								<!-- VERSION DESKTOP -->
							<?php endif ?>

						<div class="mt-5vh text-center op-0">
							<h2 class="text-white fs-48 fw-700"><?php the_title(); ?></h2>
							<p class="text-white fs-22 mb-5vh">Page en construction</p>
							<p class="text-white fs-21 mb-5vh"><?=  get_field('text_page_building'); ?><p>
							<a class="btn-airmarine-white anim-300" href="<?=  get_site_url(); ?>">
								Retour à l'accueil
							</a>
						</div>
					</div>
		    	</div>
			</div>
		</div>
	</div>
	</div>
<!-- END BUILDING MODE -->
