<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

?>


<li <?php post_class('archive-single-product col-12 col-md-6'); ?> id="post-<?php the_ID() ?>">
    <a href="<?= get_permalink(); ?>">
        <div class="product-inner-wrapper">
            <?= get_the_post_thumbnail(); ?>
            <div class="product-title">
                <div class="col">
                    <p class="w-100"><?= get_the_title(); ?></p>
                    <?php if(get_post_type() != 'product' && get_post_type_object(get_post_type())->type == 'produit' && get_field('product_price', get_the_ID())): ?>
                        <p><?= get_field('product_price', get_the_ID()); ?> €</p>
                    <?php endif; ?>

                </div>

                <div class="anim-300 p-2 mb-0">
                    <img src="<?=  get_stylesheet_directory_uri() . "/assets/img/arrow.svg"; ?>" alt="">
                </div>
            </div>
        </div>
    </a>
</li>