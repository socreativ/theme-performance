<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

$taxo = get_the_terms(get_the_ID(), get_field('cpt', 'options')['slug'].'_category');
$taxo = array_filter($taxo, function($t){return $t->parent !== 0;});
usort($taxo, function($a, $b){return $b->parent - $a->parent;});

$taxoFilter = [];
foreach ($taxo as $el) {
        $taxoFilter[get_term($el->parent)->name][$el->name] = get_term_link($el->term_id);
}

$related_taxo = $taxo[rand(0, count($taxo)-1)];
$rp_query = new WP_Query(array(
    'post_type' => get_post_type(),
    'posts_per_page' => 4,
    'tax_query' => array( array(
        'taxonomy' => $related_taxo->taxonomy,
        'field' => 'slug',
        'terms' => $related_taxo->slug,
    )),
));
$related_posts = $rp_query->posts;
foreach ($related_posts as $id => $r_post) {
    if($r_post->ID == get_the_ID()) unset($related_posts[$id]);
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cpt-single'); ?>>
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6">
                <div class="product-gallery-container">
                    <div class="product-gallery-sticky">
                        <?php $images = get_field('galerie', get_the_ID()); ?>
                        <?php foreach( $images as $image ): ?>
                            <a data-fancybox="product-gallery" href="<?=  esc_url($image['url']); ?>"><img src="<?=  esc_url($image['url']); ?>" alt="<?=  esc_attr($image['alt']); ?>" /></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 product-information">
                <a class="back-to-archive mb-5 d-block" href="<?= get_post_type_archive_link(get_post_type()); ?>"><img src="<?= get_stylesheet_directory_uri() . "/assets/img/left-arrow.svg"; ?>">Retour</a>
                <h1 class="product-name"><?= the_title(); ?></h1>
                <p class="post-taxo">
                    <?php $i=0; foreach($taxoFilter as $parent => $children): ?>

                        <?= $parent . " : " ?>

                        <?php $j =0; foreach($children as $name => $link): ?>
                            <a href="<?= $link; ?>"><?= $name; ?><?php if(count($children) > 1 && $j != count($children) -1) echo ', '; ?></a>
                        <?php $j++; endforeach; ?>
                        
                        <?php if(count($taxoFilter) > 1 && $i != count($taxoFilter) - 1): if(!my_wp_is_mobile()): echo ' | '; else: echo '<br>'; endif; endif;?>
                    <?php $i++; endforeach; ?>
                </p>
                <?php if(get_post_type_object(get_post_type())->type == 'produit' && get_field('product_price', get_the_ID())):
                    $promo = get_field('promotion', get_the_ID());
                    $price = get_field('product_price', get_the_ID()); ?>
                    <div class="product-price">
                        <?php if($promo): ?><p class="new-price"><?= $price - $promo ?>€</p><?php endif; ?>
                        <p <?php if($promo) echo 'class="old-price"'; ?>><?= $price; ?>€</p>
                        
                    </div>
                    <a class="buy" href="<?= get_field('link', get_the_ID()); ?>" target="_blank"><img src="<?= get_stylesheet_directory_uri() . "/assets/img/bag-09.svg"; ?>" alt="cart svg"> Acheter</a>

                <?php endif; ?>

                <div class="product-desc">
                    <ul class="tabs">
                        <li class="desc-tab-tgl ml-0 active">Description</li>
                        <?php if(get_post_type_object(get_post_type())->type == 'produit' && get_field('more_info')): ?>
                        <li class="additional-tab-tgl">Informations additionnelles</li>
                        <?php endif; ?>
                    </ul>
                <div class="desc-tab">
                    <?php the_content(); ?>
                </div>

                <?php if(get_post_type_object(get_post_type())->type == 'produit' && get_field('more_info')): ?>
                <div class="additional-tab d-none">
                    <table class="attributs">
                        <tbody>
                            <?php if(get_field('ref', get_the_ID())): ?>
                            <tr>
                                <th>Référence</th>
                                <td><?= get_field('ref', get_the_ID()); ?></td>
                            </tr>
                            <?php endif; ?>
                            <?php if(get_field('product_date', get_the_ID())): ?>
                            <tr>
                                <th>Date</th>
                                <td><?= get_field('product_date'); ?></td>
                            </tr>
                            <?php endif; ?>
                            <?php if(get_field('product_poids', get_the_ID())): ?>
                            <tr>
                                <th>Poids</th>
                                <?php if(get_field('product_poids', get_the_ID())[0] == '0'): ?>
                                    <td><?= substr(get_field('product_poids', get_the_ID()), 2); ?> (g) </td>
                                <?php else: ?>
                                    <td><?= get_field('product_poids', get_the_ID()); ?> (kg) </td>
                                <?php endif; ?>
                            </tr>
                            <?php endif; ?>
                            <?php if(get_field('product_dimensions', get_the_ID())['h']): ?>
                            <tr>
                                <th>Dimensions</th>
                                <?php $dim = get_field('product_dimensions', get_the_ID()); ?>
                                <td><?= 'H'.$dim['h'] . ' x ' . 'L'.$dim['L']  . ' x ' . 'P'.$dim['P']; ?> (cm)</td>
                            </tr>   
                            <?php endif; ?>                        
                            <?php foreach ($taxoFilter as $parent => $each): ?>
                            <tr>
                                <th><?= $parent ?></th>
                                
                                <td>
                                    <?php $i=0; foreach($each as $name => $link): ?>
                                        <a href="<?= $link; ?>"><?= $name; ?></a><?php if(count($each) > 1 && $i != count($each) -1) echo ', '; ?>
                                    <?php $i++; endforeach; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php if(get_field('product_price', get_the_ID())): ?>
                            <tr>
                                <th>Livraison</th>
                                <td><?= $livr = get_field('livraison') ? 'Oui' : 'Non'; ?></td>
                            </tr>
                            <?php endif; ?>
                            <?php if(get_field('livraison')): ?>
                            <tr>
                                <th>Info livraison</th>
                                <td><?= get_field('livraison_info'); ?></td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>

                    <?php if(get_field('free_space')): ?>
                        <div>
                            <?= get_field('free_space'); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>

            </div>

            <?php if($related_posts): ?>
            <div class="related-posts">
                <h2 class="similar-posts">Posts similaires lié à <a href="<?= get_term_link($related_taxo->term_id); ?>"><?= $related_taxo->name ?></a> : </h2>   
                <div class="row">
                    <?php foreach($related_posts as $c_post): ?>         
                        <a class="product-inner-link" href="<?= get_permalink(); ?>">
                            <div class="product-inner-wrapper">
                                <img class="attachment-post-thumbnail wp-post-image" src="<?= get_the_post_thumbnail_url($c_post->ID) ?>" alt="post thumbnail" />
                                <div class="product-title">
                                    <div class="d-flex">
                                        <p class="w-100"><?= $c_post->post_title ?></p>
                                        <img src="<?= get_stylesheet_directory_uri() . "/assets/img/arrow.svg"; ?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>

                <a class="see-all" href="<?= get_term_link($related_taxo->term_id); ?>">Voir tout</a>

            </div>
            <?php endif; ?>

            <?php $featured_posts = get_field('same_product'); ?>
            <?php if($featured_posts): ?>
            <div class="see-more-posts">
                <h2 class="see-more">Voir aussi : </h2>   
                <div class="row">
                    <?php foreach($featured_posts as $c_post): ?>
                        <a class="product-inner-link" href="<?= get_permalink(); ?>">
                            <div class="product-inner-wrapper">
                                <img class="attachment-post-thumbnail wp-post-image" src="<?= get_the_post_thumbnail_url($c_post->ID) ?>" alt="post thumbnail" />
                                <div class="product-title">
                                    <div class="col">
                                        <p class="w-100"><?= $c_post->post_title ?></p>
                                        <?php if(get_post_type_object(get_post_type())->type == 'produit' && get_field('product_price', $c_post->ID)): ?>
                                            <p><?= get_field('product_price', $c_post->ID); ?> €</p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="anim-300 p-2 mb-0">
                                        <img src="<?= get_stylesheet_directory_uri() . "/assets/img/arrow.svg"; ?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php endif; ?>

            <div style="height:5vh;"></div>
            <p>Vous êtes intéressé ? <a class="bold" href="<?= get_home_url(); ?>?ref=<?= get_field('ref', get_the_ID()); ?>#contact">Contactez-nous</a></p>
            <div style="height:5vh;"></div>

        </div>
    </div>
	
</article><!-- #post-<?php the_ID(); ?> -->
