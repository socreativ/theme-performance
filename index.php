<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

get_header();

$bkg = get_field('other_styles', 'options')['blog'] ? 'primary-dark' : 'black';

?>


	
<main class="mh-100vh has-black-background-color">

	<?php 
		$last_posts = wp_get_recent_posts(array( 'numberposts' => 1,'post_status' => 'publish'));
		$img = get_the_post_thumbnail_url($last_posts->ID);
		echo '<img class="attachment-post-thumbnail" src="'. $img .'">';
	?>

	<div class="has-<?= $bkg; ?>-background-color blog-background"></div>

	<div class="blog-title-side text-white team-title ">
			<div class="row">
					<div class="col">
							<?php single_post_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div>
			</div>
			<div class="row d-none d-md-flex">
					<div class="col">
						<ul class="list-unstyled cat-aside">
							<?php wp_list_categories(); ?>
							<li class="current-cat"><a href="<?= get_permalink( get_option( 'page_for_posts' ) ); ?>">Toutes</a></li>
						</ul>
					</div>
			</div>
	</div>


	<div class="container pt-25vh pt-sm-5vh">
		<div class="row justify-content-end">
			<div class="col-12 col-md-8 container-index">
				<div class="d-flex flex-wrap justify-content-start">
					<?php
					if ( have_posts() ) :
						$i = 0;
						if ( is_home() && ! is_front_page() ) :
						endif;
						/* Start the Loop */

						while ( have_posts() ) :
							the_post();
							/*
							* Include the Post-Type-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Type name) and that will be used instead.
							*/
							get_template_part( 'template-parts/content-blog', get_post_type(), array($i) );
							$i++;
						endwhile;
					else :
						get_template_part( 'template-parts/content', 'none' );
					endif;
					?>
					<div class="opacity-appear navigation-div">
						<?php the_posts_navigation(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php
get_sidebar();
get_footer();
