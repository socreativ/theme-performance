<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package theme-by-socreativ
 */

get_header();

$bkg = get_field('other_styles', 'options')['archive_cpt'] ? 'primary-dark' : 'black';

?>

<main class="mh-100vh has-black-background-color ">

	<img class="attachment-post-thumbnail archive-bkg" src="<?= get_field('404', 'options')['bkg']['url']; ?>">
	<div class="has-<?= $bkg; ?>-background-color blog-background"></div>

	<div class="container pt-25vh">

		<ul class="products justify-content-between row p-0">
		<?php if ( have_posts() ) : ?>
			

			<div class="search-header">
				<h1 class="page-title">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Résulats pour: %s', 'theme-by-socreativ' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</div><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		</ul>

	</div>

	</main>

<?php
get_sidebar();
get_footer();
