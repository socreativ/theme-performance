<?php

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

function addCartLinkToMenu( $items, $args ) {
	if ( $args->theme_location == 'main-menu' && my_wp_is_mobile() ) {
		$items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-cart"><a href="'.wc_get_cart_url().'">'.__('Cart', 'woocommerce').'</a>';
	}
	return $items;
}
add_action('wp_nav_menu_items', 'addCartLinkToMenu' , 10, 2);

