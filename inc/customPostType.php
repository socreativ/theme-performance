<?php

    if ( ! function_exists( 'create_post_type' ) ) :

        function create_post_type() {

            $slug = get_field('cpt', 'options')['slug'];
            $sing = get_field('cpt', 'options')['singulier'];
            $plu = get_field('cpt', 'options')['all'];
            $dashicon = get_field('cpt', 'options')['dashicon'];
            $type = get_field('type', 'options') ? 'produit' : 'article';
            $taxo = get_field('taxo', 'options');

            $labels = array(
                'name' => $sing,
                'all_items' => 'Tous les ' . $plu,
                'singular_name' => $sing,
                'add_new' => 'Ajouter',
                'menu_name' => $plu,
                'archives' => $plu
            );
            
            $args = array(
                'labels' => $labels,
                'public' => true,
                'show_in_rest' => true,
                'has_archive' => true,
                'supports' => array ( 'title', 'editor', 'custom-fields', 'page-attributes', 'thumbnail', 'revisions', 'excerpt' ),
                'menu_position' => 30, 
                'menu_icon' => $dashicon,
                'type' => $type
            );
        
            register_post_type( $slug, $args);
        
            // Déclaration de la Taxonomie
            $labels = array(
                'name' => $taxo,
                'add_new_item' => 'Ajouter un nouvel élément',
                'parent_item' => 'Type de l\'élement parent',
            );
        
            $args = array( 
                'labels' => $labels,
                'public' => true, 
                'show_in_rest' => true,
                'hierarchical' => true, 
                'has_archive' => true,
                'show_admin_column' => true
            );
        
            register_taxonomy( $slug.'_category', $slug, $args );
        
        }
        add_action( 'init', 'create_post_type' );
        
        endif;

?>