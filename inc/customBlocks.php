<?php 

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

         // Espacement
         acf_register_block_type(array(
            'name'              => 'spacing',
            'title'             => __('Espacement'),
            'description'       => __('margin block'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-break',
            'keywords'          => array( 'margin', 'space', 'vh'),
            'render_template'   => '/template-parts/blocks/spacer/spacer.php',
            'supports'          => array( 'anchor' => true),
        ));
        acf_register_block_type(array(
            'name'              => 'Slider',
            'title'             => __('Slider'),
            'description'       => __('Slider'),
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'slider', 'video', 'picture'),
            'render_template'   => '/template-parts/blocks/slider/slider.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/slider/slider.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/slider/slider.js',

        ));
        acf_register_block_type(array(
            'name'              => 'Slider_Filter',
            'title'             => __('Slider_Filter'),
            'description'       => __('Slider_Filter'),
            'category'          => 'custom-blocks',
            'icon'              => 'embed-photo',
            'keywords'          => array( 'Nos réalisation', 'slider', 'modèles piscine', 'Filtre', 'Ajax'),
            'render_template'   => '/template-parts/blocks/slider-filter/slider-filter.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/slider-filter/slider-filter.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/slider-filter/slider-filter.js',

        ));
        acf_register_block_type(array(
            'name'              => 'Etapes',
            'title'             => __('Etapes'),
            'description'       => __('Etapes'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-ol',
            'keywords'          => array( 'Repeteur', 'Etapes'),
            'render_template'   => '/template-parts/blocks/etapes/etapes.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/etapes/etapes.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/etapes/etapes.js',
        ));
        acf_register_block_type(array(
            'name'              => 'toggle',
            'title'             => __('Toggle'),
            'description'       => __('Toggle slider'),
            'category'          => 'custom-blocks',
            'icon'              => 'groups',
            'keywords'          => array( 'toggle'),
            'render_template'   => '/template-parts/blocks/toggle/toggle.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/toggle/toggle.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/toggle/toggle.js',
        ));
        acf_register_block_type(array(
            'name'              => 'timeline',
            'title'             => __('Timeline'),
            'description'       => __('Timeline de dates'),
            'category'          => 'block-airmarine',
            'icon'              => 'calendar-alt',
            'keywords'          => array('timeline', 'date'),
            'render_template'   => '/template-parts/blocks/timeline/timeline.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/timeline/timeline.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/timeline/timeline.js',
        ));
        acf_register_block_type(array(
            'name'              => 'partenaires',
            'title'             => __('Partenaire Slider'),
            'description'       => __('Partenaire Slider'),
            'category'          => 'custom-blocks',
            'icon'              => 'groups',
            'keywords'          => array( 'Partenaire Slider', 'Slider', 'Partenaire', 'Logo'),
            'render_template'   => 'template-parts/blocks/partenaires/partenaires.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/partenaires/partenaires.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/partenaires/partenaires.js',
        ));
        acf_register_block_type(array(
            'name'              => 'parallaxImg',
            'title'             => __('Image Parallax'),
            'description'       => __('Image Parallax'),
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'parallaxImg', 'Parammax'),
            'render_template'   => 'template-parts/blocks/parallaxImg/parallaxImg.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/parallaxImg/parallaxImg.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/parallaxImg/parallaxImg.js',
        ));
        acf_register_block_type(array(
            'name'              => 'profil',
            'title'             => __('Profil'),
            'description'       => __('Profil'),
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'parallaxImg', 'Parammax'),
            'render_template'   => 'template-parts/blocks/profil/profil.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/profil/profil.css',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/profil/profil.js',
        ));

        add_action('acf/init', 'register_acf_block_types');

    }


?>