<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

get_header();

$bkg = get_field('other_styles', 'options')['archive_cpt'] ? 'primary-dark' : 'black';

?>

<main class="mh-100vh has-black-background-color ">

	<?php 
		$cpt = get_post_type();
		if(!$cpt) $cpt = 'product';
		$last_posts = wp_get_recent_posts(array( 'numberposts' => 1,'post_status' => 'publish', 'post_type' => $cpt,), 'OBJECT')[0];
		$img = get_the_post_thumbnail_url($last_posts->ID);
		if(!$img) $img = get_field('404', 'options')['bkg']['url'];
		echo '<img class="attachment-post-thumbnail archive-bkg" src="'. $img .'">';
	?>

	<div class="has-<?= $bkg; ?>-background-color blog-background"></div>

	<div class="archive-content p-0 pt-25vh pb-25vh container">
		<h1 class="archive-title">
			<?php if(function_exists('is_product_category')){
				echo is_product_category() ? get_queried_object()->name : get_post_type_object($cpt)->labels->archives;
			}
			else{
				echo get_post_type_object($cpt)->labels->archives;
			} ?>
		 </h1>

		<ul class="products row p-0">
			<?php if ( have_posts() ) : $i = 0; ?>


				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();


					get_template_part( 'template-parts/content-archive', $cpt );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
		</u>
	</div>

	<div class="filtre">
		<div class="filtre-toggle">
			<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><title>ic_add_48px</title>
				<g fill="#ffffff" class="nc-icon-wrapper">
					<path d="M38 26H26v12h-4V26H10v-4h12V10h4v12h12v4z"></path>
				</g>
			</svg>
		</div>

		<div class="filtre-sidebar open">
			<?php if(is_shop() || is_product_category()){
				echo return_terms_index('product_cat', 'taxo-filter', 'subtaxo-filter');
			} 
			else{
				echo return_terms_index(get_field('cpt', 'options')['slug'].'_category', 'taxo-filter', 'subtaxo-filter');
			} ?>
		</div>
			
	</div>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer(null, array('css' => 'd-none'));
